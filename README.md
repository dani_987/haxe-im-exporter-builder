# Haxe Stream Structure Converter Builder

Definies File-Formats and allows you to convert from and into them.

## Install

Install using haxelib:

`haxelib install im-exporter-builder`

## Predefined FileFormats

You can find predefined file formats in `hx.io.converter.format`:

### Public Formats

#### `JsonFileFormat`

Use the static function `get(strict=true)` in `hx.io.converter.format.JsonFileFormat`
conversions using the JSON-Format. By setting the srict-Parameter to false, `,` are optional and
`/*Comments*/` can be used.

### Non-Public Formats

#### `HaxeBinFormat`

Defines a simple Binary Format for saving Enums, Lists, Maps and basic Types.
There is no difference in File between Arrays and Lists, Maps and Structs.
Use Typed-Converters for this format.

#### `HaxeExtendedDynamicFormat`

Works like HaxeBinFormat, but is extended for saving additional data, to reconstruct the
original data. It can be used in Typed- and Non-Typed-Converters.

## Converters

Converters allows you to read or write using a FileFormat. There are 2 converters:

### Typed-Converter `hx.io.converter.TypedStructureConverter`

This Converter returns a Typed object. Sample:

```
var converter = new hx.io.converter.TypedStructureConverter<SomeType>(fileFormat);
switch(converter.parse(someInput)){
    case Success(data):
        //data is of Type SomeType
    case WithError(parsingError):
        //Data in someInput wasn't correct
        trace(someInput.message);
    case WithDataError(conversionError):
        //Data in someInput was ok, but couldn't be converted to SomeType
        trace(conversionError);
}
switch(converter.write(someOutput, someData)){
    case None:
        //no errors
    case Some(errorMessage):
        //was not succesful
        trace(errorMessage);
}
```

### TODO: `hx.io.converter.StructureConverter`

### Non-Typed-Converter using `hx.io.converter.DynamicStructureTools`

To convert to a dynamic type, you need to use the `hx.io.converter.FileAccessor` and cast the
data using `hx.io.converter.DynamicStructureTools`.

```
using hx.io.converter.DynamicStructureTools;
//...
var converter = new hx.io.converter.FileAccessor(fileFormat);
switch(converter.parseFromInput(someInput)){
    case Success(valueData):
        var data : Any = valueData.toAny();
        //Now you can use data
    case WithError(parsingError):
        //Data in someInput wasn't correct
        trace(someInput.message);
}
switch(converter.writeToOutput(someOutput, someData.toValue())){
    case None:
        //no errors
    case Some(errorMessage):
        //was not succesful
        trace(errorMessage);
}
```
## Define your own File-Formats

You can simply define your onw file formats. There use the enum `hx.io.converter.FileFormat`.
When converting using this library, you have on one side the enum
`hx.io.converter.FileFormat.Value`, on the other side an In- or Output. You use
`hx.io.converter.ValueType.*` to convert between them (Tokenize). The FileFormat defines
the grammar.

### `hx.io.converter.ValueType.*`

All the read Values are packed into `hx.io.converter.FileFormat.Value`.

Binary:
 - `Int8BitValueType`: Reads/Writes Binary 8-Bit (1-Byte) Integer
 - `Int32BitValueType`: Reads/Writes Binary 32-Bit (4-Byte) Integer (=`Int`)
 - `Int64BitValueType`: Reads/Writes Binary 64-Bit (8-Byte) Integer (=`haxe.Int64`)
 - `Float32BitValueType`: Reads/Writes Binary 32-Bit (4-Byte) Float
 - `Float64BitValueType`: Reads/Writes Binary 64-Bit (8-Byte) Float (=`Float`)

 Text:
  - `ConstLenStringValueType`: Reads/Writes Fixed-Length Strings (=`String`)
  - `HexStringToIntValueType`: Reads/Writes Hex-Encoded Int (=`Int`)
  - `IntFromTextValueType`: Reads/Writes Decimal-Encoded Int (=`Int`)
  - `FloatFromTextValueType`: Reads/Writes Decimal-Encoded Float (=`Float`)
  - `RegexStringValueType`: Reads/Writes Strings, as long they match the Regex (=`String`)

### `hx.io.converter.FileFormat`

 - `ConstVal(type: ValueType, value: Value)`: Requires, that `type` reads a values equals to `value`. Does not return a Value.
 - `ConstValConverter(type: ValueType, value: Value, result: Value)`: Requires, that `type` reads a values equals to `value`, if so it returns `result`.
 - `ConvertValue(type: ValueType)`: Use `type` to read some value
 - `OneOf(of: Array<FileFormat>)`: Requires that one of the `of`-Formats could be used. Returns some `Value`.
 - `AllOrdered(ordered: Array<FileFormat>)`: Requires that each of the formats are processed, each one after another. Returns an `ArrVal(Array<Value>)`.
 - `Optional(optional: FileFormat)`: Returns the Value of `optional`, if the format is allowed, else does not return a Value.
 - `RepeatCount(toRepeat: FileFormat, count: Int)`: Repeat `toRepeat` exactly `count` times, and returns `ArrVal(Array<Value>)`.
 - `RepeatMax(toRepeat: FileFormat)`: Repeats as much as possible `toRepeat` (may be 0 times) and returns `ArrVal(Array<Value>)`.
 - `Merge(toMerge: FileFormat, merger: DataOperationFunction)`: Transfors the result of `toMerge` using `merger`, and returns the result of `merger`.
 - `DynamicFormat(basedOn:FileFormat, generate: (Option<Value>)->OneOf<FileFormat,Array<String>>)`: After reading `basedOn`, the Value is passed to `generate`, and the output of `generate` is used, to process parsing. Returns both results in `ArrVal(Array<Value>)`.
 - `ReadIgnoringBetweenTokens(applyOn: FileFormat, toIgnore: ValueType, ?defaultValue:Value)`: Setups some Ignore-Tokens, which are ignored betwenn all `ValueType`. Returns the Value of `applyOn`
 - `ReadWithoutIgnoring(applyOn: FileFormat, toIgnore: ValueType)`: Undo the ignoring of `ReadIgnoringBetweenTokens`. Returns the Value of `applyOn`.
 - `IgnoreOnce(toIgnore: ValueType, ?defaultValue:Value)`: Ignores at this position a Value. Uses `defaultValue` when format is used for creating output. Returns nothing.

### Merger Functions in `hx.io.converter.FileFormat.FileFormatTools`

Often you have to Merge the Data using `Merge`, to get the correct output in the end.
Here you have some default-Mergers that are often used:

 - `FileFormatTools.getSingleItemListToItemMapper()`: Returns a Mapper, that maps `ArrVal([something])` to `something` (and back on creating output)
 - `FileFormatTools.getListToMapMerger()`: Returns a Mapper, that maps `ArrVal(Array<ArrVal([key,value])>)` to `MapVal(Array<{key:key,value:value}>)`
 - `FileFormatTools.getListToEnumMerger()`: Returns a Mapper, that maps an 2 or 3 Element Array (params are optional) to an `EnumValue`
 - `FileFormatTools.getOptPlusListToListMerger()`: Returns a Mapper, that converts `ArrVal([v,ArrVal(arr)])` to `ArrVal(v, arr)`
 - `FileFormatTools.getStringMerger()`: Returns a Mapper, that converts a list of characters to a String
 - `FileFormatTools.getDynamicLenOperator()`: Returns a Mapper, that maps the Array, List or String-Length into the first element of an array, and places the value as second. Use it in combination with `DynamicFormat` and `RepeatCount`.
 - `FileFormatTools.getListOrderChanger(len:Int,newOrder:Array<Int>)`: Returns a Mapper, that maps a List of length `len` to another List of length `len`, where the Elements are reordered. If during **parsing** a list `[a,b,c]` is created and the given parameters are `3, [1,0,2]` the mapped list will be `[b,a,c]`.

### Samples

#### Reading a Json-Like-String

To Read a String we need a double-quote, the string itself and a double qoute. So
we can start writing:

```
var stringData : FileFormat = //TODO
var wholeString = AllOrdered([
    ConstVal(new ConstLenStringValueType(1), StrVal('"')),
    stringData,
    ConstVal(new ConstLenStringValueType(1), StrVal('"')),
]);
```

Because we have used AllOrdered we get an `ArrVal([StrVal])` and not a `StrVal`, that we want.
So we have to extract it using a merger:

```
var result = Merge(wholeString, FileFormatTools.getSingleItemListToItemMapper());
```

So only the `stringData` is missing. Lets start with some Escape-Sequences:

```
var escapedStringChar = OneOf([
    ConstValConverter(new ConstLenStringValueType(2), StrVal('\\"'), StrVal('"')),
    ConstValConverter(new ConstLenStringValueType(2), StrVal('\\\\'), StrVal('\\')),
    //...
    ConstValConverter(new ConstLenStringValueType(2), StrVal('\\t'), StrVal("\t")),
]);
```

Now we can add some normal ASCII-Characters:
```
ConvertValue(new RegexStringValueType(~/[ !#-\[\]-}]/, 1))
```

Now lets add some Unicode-Escaped-Characters. Therefore we need `\u` and the 4-Digit Hex-Code:
```
AllOrdered([
    ConstVal(new ConstLenStringValueType(2), StrVal('\\u')),
    ConvertValue(new HexStringToIntValueType(4)),
])
```

Because we have used `AllOrdered`, we need to extract our Value using a merger.

At least add a Fallback of encoded Unicode-Caracters, and merge all this together:
```
var character = OneOf([
    escapedStringChar,
    //Normal characters
    ConvertValue(new RegexStringValueType(~/[ !#-\[\]-}]/, 1)),
    //Escape using Unicode
    Merge(AllOrdered([
        ConstVal(new ConstLenStringValueType(2), StrVal('\\u')),
        ConvertValue(new HexStringToIntValueType(4)),
    ]), FileFormatTools.getSingleItemListToItemMapper()),
    //Fallback vor Converting
    ConvertValue(new RegexStringValueType(~/[^"\\]/, 1)),
]);
```

To create a full string, we need to read chars as long as possible and then create a String
from the character-list:

```
var stringData : FileFormat = Merge(RepeatMax(character), FileFormatTools.getStringMerger());
```

And we have defined, how to parse and how to create a Json-conform-encoded String!

You can check the compressed example here:
`hx.io.converter.format.JsonFileFormat.getStringFormat()`. You can find there the usage
of `ReadWithoutIgnoring` because inside the string spaces should not be ignored (like outside a string).

#### More Samples

You can find some samples in `hx.io.converter.format.*`.
