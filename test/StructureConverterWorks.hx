package test;

import hx.type.DecodedType;
import hx.string.StringBuilder;
import hx.streams.StringOutput;
import hx.io.converter.ConvertingResult;
import haxe.io.Bytes;
import haxe.ds.Option;
import hx.streams.StringInput;
import hx.io.converter.format.JsonFileFormat;
import hx.io.converter.format.HaxeBinFormat;
import hx.io.converter.StructureConverter;
import buddy.BuddySuite;

using buddy.Should;
using StringTools;

enum SomeTestEnumTypedConverter {
    SomeValueTest1;
    SomeValueTest2(str: String);
    SomeValueTest3(i: Int, enumData: SomeTestEnumTypedConverter);
}

class StructureConverterWorks extends BuddySuite {
    private final enumName_SomeTestEnumTypedConverter = "test.SomeTestEnumTypedConverter";
    private static function getIntBytes(ofInt: Int) : Bytes{
        var b = Bytes.alloc(4);
        b.setInt32(0, ofInt);
        return b;
    }

    public function new() {
        describe("The StructureConverter", {
            it("can read and write Json-Strings", {
                var someString = "Some Simple String";
                var someJsonString = '"$someString"';
                var converter = new StructureConverter( new DecodedType<String>().get(), JsonFileFormat.get() );
                var res = converter.parse(new StringInput(Bytes.ofString(someJsonString)));
                '$res'.should.be('${Success(someString)}');
                var out = new StringOutput();
                converter.write(out, someString).should.equal(None);
                out.getString().should.be(someJsonString);
            });
            it("can read and write Json-Objects with bools, arrays, lists and numbers", {
                var list = new List();
                list.add(true);
                var data = {foo:[2,5],bar:true,fl:1.0,list:list};
                var someJsonString = '{"bar":true,"fl":1.0,"foo":[2,5],"list":[true]}';
                var converter = new StructureConverter( new DecodedType<{bar:Bool,foo:Array<Int>,fl:Float,list:List<Bool>}>().get(), JsonFileFormat.get() );
                var res = converter.parse(new StringInput(Bytes.ofString(someJsonString)));
                '$res'.should.be('${Success(data)}');
                var out = new StringOutput();
                converter.write(out, data).should.equal(None);
                out.getString().should.be(someJsonString);
            });
            it("can read and write Json-Maps", {
                var data = ["hi"=>5,"hallo"=>6];
                var someJsonString = '{"hi":5,"hallo":6}';
                var converter = new StructureConverter( new DecodedType<Map<String,Int>>().get(), JsonFileFormat.get() );
                var res = converter.parse(new StringInput(Bytes.ofString(someJsonString)));
                '$res'.should.be('${Success(data)}');
                var out = new StringOutput();
                converter.write(out, data).should.equal(None);
                out.getString().should.be(someJsonString);
            });
            it("can read and write HxBD-Enums with no params", {
                var dataBuilder = new StringBuilder();
                dataBuilder.add("HxBD");
                dataBuilder.add("E");
                dataBuilder.addBytes(getIntBytes(enumName_SomeTestEnumTypedConverter.length));
                dataBuilder.add(enumName_SomeTestEnumTypedConverter);
                dataBuilder.addBytes(getIntBytes("SomeValueTest1".length));
                dataBuilder.add("SomeValueTest1");
                dataBuilder.addBytes(getIntBytes(0));
                var data : SomeTestEnumTypedConverter = SomeValueTest1;
                var converter = new StructureConverter( new DecodedType<SomeTestEnumTypedConverter>().get(), HaxeBinFormat.get() );
                var res = converter.parse(new StringInput(dataBuilder.getBytes()));
                trace(res);
                '$res'.should.be('${Success(data)}');
                trace(res);
                var out = new StringOutput();
                converter.write(out, data).should.equal(None);
                out.getBytes().toHex().should.be(dataBuilder.getBytes().toHex());
            });
            it("can read and write HxBD-Enums params", {
                var dataBuilder = new StringBuilder();
                dataBuilder.add("HxBD");
                dataBuilder.add("E");
                dataBuilder.addBytes(getIntBytes(enumName_SomeTestEnumTypedConverter.length));
                dataBuilder.add(enumName_SomeTestEnumTypedConverter);
                dataBuilder.addBytes(getIntBytes("SomeValueTest2".length));
                dataBuilder.add("SomeValueTest2");
                dataBuilder.addBytes(getIntBytes(1));
                dataBuilder.add("S");
                dataBuilder.addBytes(getIntBytes("Test".length));
                dataBuilder.add("Test");
                var data : SomeTestEnumTypedConverter = SomeValueTest2("Test");
                var converter = new StructureConverter( new DecodedType<SomeTestEnumTypedConverter>().get(), HaxeBinFormat.get() );
                var res = converter.parse(new StringInput(dataBuilder.getBytes()));
                '$res'.should.be('${Success(data)}');
                trace(res);
                var out = new StringOutput();
                converter.write(out, data).should.equal(None);
                out.getBytes().toHex().should.be(dataBuilder.getBytes().toHex());
            });
            it("can read and write recursive HxBD-Enums", {
                var dataBuilder = new StringBuilder();
                dataBuilder.add("HxBD");
                dataBuilder.add("E");
                dataBuilder.addBytes(getIntBytes(enumName_SomeTestEnumTypedConverter.length));
                dataBuilder.add(enumName_SomeTestEnumTypedConverter);
                dataBuilder.addBytes(getIntBytes("SomeValueTest3".length));
                dataBuilder.add("SomeValueTest3");
                dataBuilder.addBytes(getIntBytes(2));
                dataBuilder.add("I");
                dataBuilder.addBytes(getIntBytes(5));
                dataBuilder.add("E");
                dataBuilder.addBytes(getIntBytes(enumName_SomeTestEnumTypedConverter.length));
                dataBuilder.add(enumName_SomeTestEnumTypedConverter);
                dataBuilder.addBytes(getIntBytes("SomeValueTest1".length));
                dataBuilder.add("SomeValueTest1");
                dataBuilder.addBytes(getIntBytes(0));
                var data : SomeTestEnumTypedConverter = SomeValueTest3(5, SomeValueTest1);
                var converter = new StructureConverter( new DecodedType<SomeTestEnumTypedConverter>().get(), HaxeBinFormat.get() );
                var res = converter.parse(new StringInput(dataBuilder.getBytes()));
                trace(res);
                '$res'.should.be('${Success(data)}');
                var out = new StringOutput();
                converter.write(out, data).should.equal(None);
                out.getBytes().toHex().should.be(dataBuilder.getBytes().toHex());
            });
        });
    }
}
