package test;

import hx.io.converter.FileFormat.Value;
import hx.io.converter.FileFormat.FileFormatTools;
import haxe.ds.Option;
import buddy.BuddySuite;
using buddy.Should;

class FileFormatToolsWorks extends BuddySuite {
    public function new() {
        describe("FileFormatTools", {
            it("should reorder Items correctly using getListOrderChanger", {
                var reorderer = FileFormatTools.getListOrderChanger(6, [2,5,3,4,0,1]);
                var ordered = [1,2,3,4,5,6];
                var reorderedValue = reorderer.onParse(ArrVal(ordered.map(e->IntVal(e))));
                switch(reorderedValue){
                    case ArrVal([IntVal(a),IntVal(b),IntVal(c),IntVal(d),IntVal(e),IntVal(f)]):
                        '${[a,b,c,d,e,f]}'.should.be("[3,6,4,5,1,2]");
                    case _: fail('Wrong format of $reorderedValue');
                }
                var backorderedValue = reorderer.onCreate(reorderedValue);
                switch(backorderedValue){
                    case Some(ArrVal([IntVal(a),IntVal(b),IntVal(c),IntVal(d),IntVal(e),IntVal(f)])):
                        '${[a,b,c,d,e,f]}'.should.be('$ordered');
                    case _: fail('Wrong format of $reorderedValue');
                }
            });
            it("should pack nd unpack SubArrays using getListToSublistedList", {
                var before : Value = ArrVal([IntVal(0),IntVal(1),IntVal(2),IntVal(3),IntVal(4),IntVal(5)]);
                var target : Value = ArrVal([IntVal(0),IntVal(1),ArrVal([IntVal(2),IntVal(3),IntVal(4)]),IntVal(5)]);
                var creator = FileFormatTools.getListToSublistedList(2,3);
                '${creator.onParse(before)}'.should.be('${target}');
                '${creator.onCreate(target)}'.should.be('${Option.Some(before)}');
            });
        });
    }
}
