package test;

import haxe.io.Bytes;
import hx.io.converter.FileFormat.FileFormatTools;
import hx.streams.StringOutput;
import hx.streams.StringInput;
import hx.io.converter.FileAccessor;
import hx.io.converter.format.JsonFileFormat;
import hx.io.converter.ValueType;
import buddy.BuddySuite;

using buddy.Should;
using hx.io.converter.DynamicStructureTools;


class FileAccessorWorks extends BuddySuite {
    private final sampleJsonString='{"sample":true,"data":[1,true,null]}';
    public function new() {
        describe("The FileAccessor", {
            it("can convert JSON", {
                var json = new FileAccessor(JsonFileFormat.get());
                var result = json.parseFromInput(new StringInput(Bytes.ofString(sampleJsonString)));
                var resultArr : Array<Dynamic> = [1,true,null];
                var resultMap : Map<String,Dynamic> = ["sample"=>true,"data"=>resultArr];
                switch(result){
                    case Success(val):
                        '${val.toAny()}'.should.be('$resultMap');
                    case WithError(error):
                        fail(error);
                }

                var output = new StringOutput();
                switch(json.writeToOutput(output, resultMap.toValue())){
                    case None:
                        output.getString().should.be(sampleJsonString);
                    case Some(err): fail(err);
                }
            });
            it("can convert DynamicFormat-Data", {
                var converter = new FileAccessor(Merge(
                    DynamicFormat(
                        Merge(AllOrdered([
                            ConvertValue(new IntFromTextValueType()),
                            ConstVal(new ConstLenStringValueType(1), StrVal(":")),
                        ]), FileFormatTools.getSingleItemListToItemMapper()),
                        (optVal) -> {
                            switch(optVal){
                                case(Some(IntVal(len))):return First(ConvertValue(new ConstLenStringValueType(len)));
                                default: return Second(["IntVal"]);
                            }
                        }
                    ), FileFormatTools.getDynamicLenOperator()));
                var resultString="Hallo\nWelt!\n\tÄhm...";
                var inputString = '${Bytes.ofString(resultString).length}:$resultString';
                var result = converter.parseFromInput(new StringInput(Bytes.ofString(inputString)));
                switch(result){
                    case Success(val):
                        val.toAny().should.be(resultString);
                    case WithError(error):
                        fail(error);
                }

                var output = new StringOutput();
                switch(converter.writeToOutput(output, resultString.toValue())){
                    case None:
                        output.getString().should.be(inputString);
                    case Some(err): fail(err);
                }
            });

            it("can convert FixedRepeat-Data", {
                var converter = new FileAccessor(RepeatMax(
                    RepeatCount(ConvertValue(new ConstLenStringValueType(1)), 2)
                ));
                var inputString="1234";
                var resultData = [["1","2"],["3","4"]];
                var result = converter.parseFromInput(new StringInput(Bytes.ofString(inputString)));
                switch(result){
                    case Success(val):
                        '${val.toAny()}'.should.be('$resultData');
                    case WithError(error):
                        fail(error);
                }
                var result = converter.parseFromInput(new StringInput(Bytes.ofString("12E")));
                switch(result){
                    case Success(_): fail("Expected some Error");
                    case WithError(_): //Ok
                }

                var output = new StringOutput();
                switch(converter.writeToOutput(output, [["1","2"],["E"]].toValue())){
                    case None: fail("Expected some Error");
                    case Some(_): //Ok
                }
            });

            it("does not have problems with spaces", {
                var json = new FileAccessor(JsonFileFormat.get());
                var result = json.parseFromInput(new StringInput(Bytes.ofString(' [ "Hallo" , "Hi" ] ')));
                switch(result){
                    case Success(val):
                        cast(val.toAny(), Array<Dynamic>).should.containExactly(["Hallo","Hi"]);
                    case WithError(error):
                        fail(error);
                }
            });
        });
    }
}
