package test;
using buddy.Should;

class MainTest implements buddy.Buddy<[
    test.FileAccessorWorks,
    test.HaxeBinFormatWorks,
    test.HaxeExtendedFormatWorks,
    test.DynamicStructureConverterWorks,
    test.TypedStructureConverterWorks,
    test.StructureConverterWorks,
    test.FileFormatToolsWorks,
]>{}
