package test;

import buddy.BuddySuite;

using buddy.Should;

using hx.io.converter.DynamicStructureTools;

enum TestEnum {
    Data(val: String);
    NoData;
}

class DynamicStructureConverterWorks extends BuddySuite {
    public function new() {
        describe("DynamicStructureTools",{
            it("should convert from and to Value", {
                var someData = [
                    "hallo" => {
                        someInt: 12,
                        someEnumData: Data("Hi"),
                        someEnumNoData: NoData,
                        isOk: false,
                        someArr: [1,2,3],
                    }
                ];
                trace(someData);
                var asValue = someData.toValue();
                trace(asValue);
                switch(asValue){
                    case MapVal([el]): trace(el);
                    case _:
                }
                var back : Map<String,Map<String,Dynamic>> = asValue.toAny();
                trace(back);
                back["hallo"].should.not.be(null);
                back["hallo"]["isOk"].should.be(someData["hallo"].isOk);
                '${back["hallo"]["someEnumData"]}'.should.be('${someData["hallo"].someEnumData}');
                back["hallo"]["someInt"].should.be(someData["hallo"].someInt);
                '${back["hallo"]["someEnumNoData"]}'.should.be('${someData["hallo"].someEnumNoData}');
                '${back["hallo"]["someArr"]}'.should.be('${someData["hallo"].someArr}');
                switch(back["hallo"]["someEnumNoData"]){
                    case Data(val): fail(val);
                    case NoData:
                }
                switch(back["hallo"]["someEnumData"]){
                    case Data(_):
                    case NoData: fail("Expected some Data");
                }
            });
        });
    }
}
