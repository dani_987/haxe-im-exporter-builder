package test;

import hx.io.converter.FileFormat.DataOperationFunction;
import hx.io.converter.format.HaxeExtendedDynamicFormat;
import haxe.ds.Option;
import hx.streams.StringInput;
import hx.io.converter.format.HaxeBinFormat;
import hx.streams.StringOutput;
import hx.io.converter.FileAccessor;
import buddy.BuddySuite;
import test.HaxeBinFormatWorks.SomeTestEnum;

using buddy.Should;
using hx.io.converter.DynamicStructureTools;

class HaxeExtendedFormatWorks extends BuddySuite {
    public function new() {
        describe("The HaxeExtendedFormat", {
            it("can convert and resore data", {
                function compareMap(m1: Map<String,Dynamic>,m2: Map<String,Dynamic>){
                    for(k=>v in m1){
                        if(!m2.exists(k))fail('$k is missing in m2');
                        '${m2[k]}'.should.be('$v');
                    }
                    for(k=>v in m2){
                        if(!m1.exists(k))fail('$k is missing in m1');
                    }
                }
                var data : Map<String,Dynamic> = [
                    "hi" => 12,
                    "true" => true,
                    "false" => false,
                    "fl" => 12.3,
                    "String" => "SomeString",
                    "someEnum" => V1(12),
                    "someArrEnum" => V3([3,5,80]),
                    "someObject" => {test:true,data:V2(12.3)},
                ];
                var output = new StringOutput();
                HaxeExtendedDynamicFormat.writeToOutput(output, data).should.equal(None);
                trace(output.getBytes().toHex());
                switch(HaxeExtendedDynamicFormat.parseFromInput(new StringInput(output.getBytes()))){
                    case Success(val):
                        trace(val);
                        compareMap(val, data);
                    case WithError(error):fail(error);
                }
            });
        });
    }
}
