package test;

import haxe.ds.Option;
import hx.streams.StringInput;
import hx.io.converter.format.HaxeBinFormat;
import hx.streams.StringOutput;
import hx.io.converter.FileAccessor;
import buddy.BuddySuite;

using buddy.Should;
using hx.io.converter.DynamicStructureTools;

enum SomeTestEnum {
    V1(i: Int);
    V2(f: Float);
    V3(a: Array<Int>);
}

class HaxeBinFormatWorks extends BuddySuite {
    public function new() {
        describe("The HaxeBinFormat", {
            it("can convert and resore data", {
                function compareMap(m1: Map<String,Dynamic>,m2: Map<String,Dynamic>){
                    for(k=>v in m1){
                        if(!m2.exists(k))fail('$k is missing in m2');
                        '${m2[k]}'.should.be('$v');
                    }
                    for(k=>v in m2){
                        if(!m1.exists(k))fail('$k is missing in m1');
                    }
                }
                var data : Map<String,Dynamic> = [
                    "hi" => 12,
                    "true" => true,
                    "false" => false,
                    "fl" => 12.3,
                    "String" => "SomeString",
                    "someEnum" => V1(12),
                    "someArrEnum" => V3([3,5,80]),
                ];
                var haxeBin = new FileAccessor(HaxeBinFormat.get());
                var output = new StringOutput();
                haxeBin.writeToOutput(output, data.toValue()).should.equal(None);
                switch(haxeBin.parseFromInput(new StringInput(output.getBytes()))){
                    case Success(val):
                        trace(val.toAny());
                        compareMap(val.toAny(), data);
                    case WithError(error):fail(error);
                }
            });
        });
    }
}
