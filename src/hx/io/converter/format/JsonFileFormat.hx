package hx.io.converter.format;

import hx.io.converter.FileFormat;
import hx.io.converter.ValueType;

class JsonFileFormat {
    private static function getSeperator(sep: String, strict: Bool) : FileFormat {
        if(strict){
            return ConstVal(new ConstLenStringValueType(sep.length), StrVal(sep));
        } else {
            return IgnoreOnce(new RegexStringValueType(new EReg(EReg.escape(sep),""),1), StrVal(sep));
        }
    };
    private static function getListOf(of:FileFormat, strict : Bool) : FileFormat {
        return Merge(Optional(AllOrdered([
            of,
            RepeatMax(Merge(AllOrdered([
                getSeperator(",", strict),
                of,
            ]), FileFormatTools.getSingleItemListToItemMapper()))
        ])), FileFormatTools.getOptPlusListToListMerger());
    }
    public static function getStringFormat() : FileFormat {
        return ReadWithoutIgnoring( Merge( AllOrdered([
            ConstVal(new ConstLenStringValueType(1), StrVal('"')),
            Merge(RepeatMax(OneOf([
                //Escaped Characters
                ConstValConverter(new ConstLenStringValueType(2), StrVal('\\"'), StrVal('"')),
                ConstValConverter(new ConstLenStringValueType(2), StrVal('\\\\'), StrVal('\\')),
                ConstValConverter(new ConstLenStringValueType(2), StrVal('\\/'), StrVal('/')),
                ConstValConverter(new ConstLenStringValueType(2), StrVal('\\b'), StrVal("\x08")),
                ConstValConverter(new ConstLenStringValueType(2), StrVal('\\f'), StrVal("\x0C")),
                ConstValConverter(new ConstLenStringValueType(2), StrVal('\\n'), StrVal("\n")),
                ConstValConverter(new ConstLenStringValueType(2), StrVal('\\r'), StrVal("\r")),
                ConstValConverter(new ConstLenStringValueType(2), StrVal('\\t'), StrVal("\t")),
                //Normal characters
                ConvertValue(new RegexStringValueType(~/[ !#-\[\]-}]/, 1)),
                //Escape using Unicode
                Merge(AllOrdered([
                    ConstVal(new ConstLenStringValueType(2), StrVal('\\u')),
                    ConvertValue(new HexStringToIntValueType(4)),
                ]), FileFormatTools.getSingleItemListToItemMapper()),
                //Fallback vor Converting
                ConvertValue(new RegexStringValueType(~/[^"\\]/, 1)),
            ])), FileFormatTools.getStringMerger()),
            ConstVal(new ConstLenStringValueType(1), StrVal('"')),
        ]), FileFormatTools.getSingleItemListToItemMapper()), new RegexStringValueType(~/./));
    }

    public static function get(strict : Bool = true) : FileFormat {
        var valuesList : Array<FileFormat> = [
            ConvertValue(new FloatFromTextValueType()),
            ConvertValue(new IntFromTextValueType()),
            ConstValConverter(new ConstLenStringValueType(4), StrVal("null"), NullVal),
            ConstValConverter(new ConstLenStringValueType(4), StrVal("true"), BoolVal(true)),
            ConstValConverter(new ConstLenStringValueType(5), StrVal("false"), BoolVal(false)),
            getStringFormat(),
        ];
        var someValue : FileFormat = OneOf(valuesList);
        var jsonListReader : FileFormat = Merge(AllOrdered([
            ConstVal(new ConstLenStringValueType(1), StrVal('[')),
            getListOf(someValue, strict),
            ConstVal(new ConstLenStringValueType(1), StrVal(']')),
        ]), FileFormatTools.getSingleItemListToItemMapper());
        valuesList.push(jsonListReader);
        var jsonMapReader : FileFormat = Merge(Merge(AllOrdered([
            ConstVal(new ConstLenStringValueType(1), StrVal('{')),
            getListOf(AllOrdered([ getStringFormat(), getSeperator(":", strict), someValue ]), strict),
            ConstVal(new ConstLenStringValueType(1), StrVal('}')),
        ]), FileFormatTools.getSingleItemListToItemMapper()), FileFormatTools.getListToMapMerger());
        valuesList.push(jsonMapReader);
        var ignorer : ValueType;
        if(strict){
            ignorer = new RegexStringValueType(~/[\s]/);
        } else {
            ignorer = FileAccessor.asValueType(RepeatMax(
                OneOf([
                    AllOrdered([
                        ConstVal(new ConstLenStringValueType(2), StrVal('/*')),
                        IgnoreOnce(new RegexStringValueType(~/(\*(?!\/)|[^*])+/), StrVal("")),
                        ConstVal(new ConstLenStringValueType(2), StrVal('*/')),
                    ]),
                    IgnoreOnce(new RegexStringValueType(~/[\s]+/), StrVal("")),
                ])
            ));
        }
        return ReadIgnoringBetweenTokens( someValue, ignorer );
    }
}
