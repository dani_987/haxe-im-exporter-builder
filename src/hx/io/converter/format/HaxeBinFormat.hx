package hx.io.converter.format;

import hx.io.converter.ValueType;
import hx.io.converter.FileFormat.FileFormatTools;

class HaxeBinFormat {
    private static var valuesList : Array<FileFormat> = [];
    public static function getFormat(start: String, converter: FileFormat) : FileFormat {
        return Merge(AllOrdered([
            ConstVal(new ConstLenStringValueType(start.length), StrVal(start)),
            converter,
        ]), FileFormatTools.getSingleItemListToItemMapper());
    }

    public static function getStringFormat() : FileFormat {
        return Merge(DynamicFormat(
            ConvertValue(new Int32BitValueType()),
            (optVal) -> {
                switch(optVal){
                    case(Some(IntVal(len))):return First(ConvertValue(new ConstLenStringValueType(len)));
                    default: return Second(["IntVal"]);
                }
            }
        ), FileFormatTools.getDynamicLenOperator());
    }

    public static function getArrayFormat(values : Array<FileFormat>) : FileFormat {
        return Merge(DynamicFormat(
            ConvertValue(new Int32BitValueType()),
            (optVal) -> {
                switch(optVal){
                    case(Some(IntVal(len))):return First(RepeatCount(OneOf(values),len));
                    default: return Second(["IntVal"]);
                }
            }
        ), FileFormatTools.getDynamicLenOperator());
    }

    public static function getMapFormat(values : Array<FileFormat>) : FileFormat {
        return Merge(Merge(DynamicFormat(
            ConvertValue(new Int32BitValueType()),
            (optVal) -> {
                switch(optVal){
                    case(Some(IntVal(len))):return First(RepeatCount(AllOrdered([
                        OneOf(values), OneOf(values),
                    ]),len));
                    default: return Second(["IntVal"]);
                }
            }
        ), FileFormatTools.getDynamicLenOperator()), FileFormatTools.getListToMapMerger());
    }

    public static function get() : FileFormat {
        if(valuesList.length == 0){
            valuesList.push( getFormat("F",ConvertValue(new Float64BitValueType())) );
            valuesList.push( getFormat("I",ConvertValue(new Int32BitValueType())) );
            valuesList.push( getFormat("J",ConvertValue(new Int64BitValueType())) );
            valuesList.push( ConstValConverter(new ConstLenStringValueType(1), StrVal("n"), NullVal) );
            valuesList.push( ConstValConverter(new ConstLenStringValueType(1), StrVal("t"), BoolVal(true)) );
            valuesList.push( ConstValConverter(new ConstLenStringValueType(1), StrVal("f"), BoolVal(false)) );
            valuesList.push( getFormat("S", getStringFormat()) );
            valuesList.push( getFormat("A", getArrayFormat(valuesList)) );
            valuesList.push( getFormat("M", getMapFormat(valuesList)) );
            valuesList.push( getFormat("E", Merge(AllOrdered([
                getStringFormat(), getStringFormat(), getArrayFormat(valuesList)
            ]), FileFormatTools.getListToEnumMerger())) );
        }
        return getFormat("HxBD",OneOf(valuesList));
    }
}
