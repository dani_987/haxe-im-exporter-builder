package hx.io.converter.format;

import haxe.ds.Option;
import haxe.EnumTools.EnumValueTools;
import hx.io.converter.FileFormat;
import hx.io.converter.ValueType;
import hx.io.converter.ConvertingResult.FileAccessorResult;

using haxe.Int64;

class HaxeExtendedDynamicFormat {
    private static var valuesList : Array<FileFormat> = [];
    private static var converter = getFileAccessor();

    public static function getFormatInData(start: String, converter: FileFormat) : FileFormat {
        return AllOrdered([
            ConstValConverter(new ConstLenStringValueType(start.length), StrVal(start), StrVal(start)),
            converter,
        ]);
    }

    private static function getFileAccessor() : FileAccessor {
        valuesList.push( HaxeBinFormat.getFormat("F",ConvertValue(new Float64BitValueType())) );
        valuesList.push( HaxeBinFormat.getFormat("I",ConvertValue(new Int32BitValueType())) );
        valuesList.push( HaxeBinFormat.getFormat("J",ConvertValue(new Int64BitValueType())) );
        valuesList.push( ConstValConverter(new ConstLenStringValueType(1), StrVal("n"), NullVal) );
        valuesList.push( ConstValConverter(new ConstLenStringValueType(1), StrVal("t"), BoolVal(true)) );
        valuesList.push( ConstValConverter(new ConstLenStringValueType(1), StrVal("f"), BoolVal(false)) );
        valuesList.push( HaxeBinFormat.getFormat("S", HaxeBinFormat.getStringFormat()) );
        valuesList.push( HaxeBinFormat.getFormat("E", Merge(AllOrdered([
            HaxeBinFormat.getStringFormat(), HaxeBinFormat.getStringFormat(), HaxeBinFormat.getArrayFormat(valuesList)
        ]), FileFormatTools.getListToEnumMerger())) );
        valuesList.push( getFormatInData("A", HaxeBinFormat.getArrayFormat(valuesList)) );
        valuesList.push( getFormatInData("L", HaxeBinFormat.getArrayFormat(valuesList)) );
        valuesList.push( getFormatInData("M", HaxeBinFormat.getMapFormat(valuesList)) );
        valuesList.push( getFormatInData("O", HaxeBinFormat.getMapFormat(valuesList)) );

        return new FileAccessor(HaxeBinFormat.getFormat("XdBD",OneOf(valuesList)));
    }

    public static function parseFromInput(input:hx.streams.LocalizedInput) : FileAccessorResult<Any> {
        switch(converter.parseFromInput(input)){
            case Success(val):return Success(valueToAny(val));
            case WithError(error): return WithError(error);
        }
    }
    public static function writeToOutput(output:hx.streams.LocalizedOutput, data: Any) : Option<String> {
        return converter.writeToOutput(output, anyToValue(data));
    }

    private static function valueToAny(data: Value) : Any {
        switch(data){
            case IntVal(val): return val;
            case Int64Val(val): return val;
            case FloatVal(val): return val;
            case StrVal(val): return val;
            case BoolVal(val): return val;
            case NullVal: return null;
            case ArrVal([StrVal("A"),ArrVal(val)]):
                return [for(el in val)valueToAny(el)];
            case ArrVal([StrVal("L"),ArrVal(val)]):
                var result = new List();
                for(el in val)result.add(valueToAny(el));
                return result;
            case ArrVal([StrVal("M"),MapVal(map)]):
                return [for(el in map)valueToAny(el.key) => valueToAny(el.value)];
            case ArrVal([StrVal("O"),MapVal(map)]):
                var result = {};
                for(el in map) switch(el.key){
                    case StrVal(k): Reflect.setField(result, k, valueToAny(el.value));
                    case _:
                }
                return result;
            case EnumObject(name, _, val, params):
                if(name != null){
                    var enumData = Type.resolveEnum(name);
                    if(enumData == null)return null;
                    var creationParams = [];
                    if(params != null)creationParams = [for(param in params)valueToAny(param)];
                    if(val != null){
                        return enumData.createByName(val, creationParams);
                    }
                }
                return null;
            case _: return null;
        }
    }

    private static function anyToValue(data: Any) : Value {
        if(data == null)return NullVal;
        else if(data is Int)return IntVal(data);
        else if(data.isInt64())return Int64Val(data);
        else if(data is Float)return FloatVal(data);
        else if(data is String)return StrVal(data);
        else if(data is Bool)return BoolVal(data);
        else if(data is Array)return ArrVal([StrVal("A"),ArrVal([for(el in cast(data,Array<Dynamic>))anyToValue(el)])]);
        else if(data is List)return ArrVal([StrVal("L"),ArrVal([for(el in cast(data,List<Dynamic>))anyToValue(el)])]);
        else if(data is haxe.Constraints.IMap){
            var mapData = cast(data, haxe.Constraints.IMap<Dynamic,Dynamic>);
            return ArrVal([StrVal("M"),MapVal([
                for(k in mapData.keys()){key:anyToValue(k), value:anyToValue(mapData.get(k))}
            ])]);
        }
        else if(Reflect.isEnumValue(data))return EnumObject(
            Type.getEnum(data).getName(),
            null,
            EnumValueTools.getName(data),
            [for(param in EnumValueTools.getParameters(data))anyToValue(param)]
        );
        else if(Reflect.isObject(data))return ArrVal([StrVal("O"),MapVal([
            for(field in Reflect.fields(data)){
                key:StrVal(field),
                value:anyToValue(Reflect.field(data,field)),
            }
        ])]);
        else {
            trace('Cannot convert $data', data, Type.getClass(data));
            return NullVal;
        }
    }
}
