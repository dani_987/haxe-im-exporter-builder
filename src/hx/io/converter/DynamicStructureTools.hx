package hx.io.converter;

import haxe.Constraints.IMap;
import hx.io.converter.FileFormat.Value;

using haxe.Int64;
using haxe.EnumTools.EnumValueTools;
using extension.tools.DefaultTools;

class DynamicStructureTools {
    public static function toValue(data: Any) : Value {
        if(data == null)return NullVal;
        else if(data is Int)return IntVal(data);
        else if(data.isInt64())return Int64Val(data);
        else if(data is Float)return FloatVal(data);
        else if(data is String)return StrVal(data);
        else if(data is Bool)return BoolVal(data);
        else if(data is Array)return ArrVal([for(el in cast(data,Array<Dynamic>))toValue(el)]);
        else if(data is List)return ArrVal([for(el in cast(data,List<Dynamic>))toValue(el)]);
        else if(data is IMap){
            var mapData = cast(data, IMap<Dynamic,Dynamic>);
            return MapVal([
                for(k in mapData.keys()){key:toValue(k), value:toValue(mapData.get(k))}
            ]);
        }
        else if(Reflect.isEnumValue(data))return EnumObject(
            Type.getEnum(data).getName(),
            EnumValueTools.getIndex(data),
            EnumValueTools.getName(data),
            [for(param in EnumValueTools.getParameters(data))toValue(param)]
        );
        else if(Reflect.isObject(data))return MapVal([
            for(field in Reflect.fields(data)){
                key:toValue(field),
                value:toValue(Reflect.field(data,field)),
            }
        ]);
        else {
            trace('Cannot convert $data', data, Type.getClass(data));
            return NullVal;
        }
    }
    public static function toAny(data: Value, createList: Null<Array<Dynamic>->Dynamic> = null, createMap: Null<Array<{key: Any, value: Dynamic}>->Dynamic> = null) : Any {
        switch(data){
            case IntVal(val): return val;
            case Int64Val(val): return val;
            case FloatVal(val): return val;
            case StrVal(val): return val;
            case BoolVal(val): return val;
            case NullVal: return null;
            case ArrVal(val): return createList.orDefault(a->a)([for(el in val)toAny(el, createList, createMap)]);
            case MapVal(val):
                return createMap.orDefault((data)->{
                    return [for(el in data)el.key => el.value];
                })([for(el in val){key: toAny(el.key, createList, createMap), value: toAny(el.value, createList, createMap)}]);
            case EnumObject(name, val, strVal, params):
                if(name != null){
                    var enumData = Type.resolveEnum(name);
                    if(enumData == null)return null;
                    var creationParams = [];
                    if(params != null)creationParams = [for(param in params)toAny(param, createList, createMap)];
                    if(val != null){
                        return enumData.createByIndex(val, creationParams);
                    }
                    if(strVal != null){
                        return enumData.createByName(strVal, creationParams);
                    }
                }
                return null;
        }
    }

    public static function mapToObject(map: Map<String,Dynamic>, mapper: Null<Dynamic->Dynamic> = null) : Dynamic {
        var obj : Dynamic = {};
        var mapperFun = mapper;
        if(mapperFun == null)mapperFun = o->o;
        for(key => value in map){
            Reflect.setField(obj, key, mapperFun(value));
        }
        return obj;
    }
}
