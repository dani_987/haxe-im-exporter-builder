package hx.io.converter;

import haxe.ds.Option;
import hx.streams.LocalizedOutput;
import haxe.Exception;
import hx.streams.LocalizedInput;
import haxe.io.Bytes;
import hx.io.converter.FileFormat;

using hx.string.StringReaderTools;
using extension.tools.OptionTools;
using StringTools;

typedef ReadValue = {
    value: Value,
    countReadBytes: Int,
    success: Bool,
}

class Int8BitValueType extends ByteValueType {
    public function new() {
        super(
            1,
            (v)->{return IntVal(v.get(0));},
            (b,v)->{
                switch(v){
                    case IntVal(val):
                        b.set(0,val);
                        return true;
                    default: return false;
                }
            }
        );
    }
    public override function getExpected():Array<String> { return ['Int4Bytes']; }
}
class Int32BitValueType extends ByteValueType {
    public function new() {
        super(
            4,
            (v)->{return IntVal(v.getInt32(0));},
            (b,v)->{
                switch(v){
                    case IntVal(val):
                        b.setInt32(0,val);
                        return true;
                    default: return false;
                }
            }
        );
    }
    public override function getExpected():Array<String> { return ['Int4Bytes']; }
}
class Int64BitValueType extends ByteValueType {
    public function new() {
        super(
            8,
            (v)->{return Int64Val(v.getInt64(0));},
            (b,v)->{
                switch(v){
                    case Int64Val(val):
                        b.setInt64(0,val);
                        return true;
                    default: return false;
                }
            }
        );
    }
    public override function getExpected():Array<String> { return ['Int8Bytes']; }
}
class Float32BitValueType extends ByteValueType {
    public function new() {
        super(
            4,
            (v)->{return FloatVal(v.getFloat(0));},
            (b,v)->{
                switch(v){
                    case FloatVal(val):
                        b.setFloat(0,val);
                        return true;
                    default: return false;
                }
            }
        );
    }
    public override function getExpected():Array<String> { return ['Float4Bytes']; }
}
class Float64BitValueType extends ByteValueType {
    public function new() {
        super(
            8,
            (v)->{return FloatVal(v.getDouble(0));},
            (b,v)->{
                switch(v){
                    case FloatVal(val):
                        b.setDouble(0,val);
                        return true;
                    default: return false;
                }
            }
        );
    }
    public override function getExpected():Array<String> { return ['Float8Bytes']; }
}
class ConstLenStringValueType extends ByteValueType {
    public function new(strLen: Int) {
        super(
            strLen,
            (v)->{return StrVal(v.getString(0, strLen));},
            (b,v)->{
                switch(v){
                    case StrVal(val):
                        b.blit(0,Bytes.ofString(val),0,strLen);
                        return true;
                    default: return false;
                }

            }
        );
    }
}

class ByteValueType extends NumberFromByteValueType {
    public function new(byteCount: Int, readFromByte: (Bytes)->Value, writeToBytes:(Bytes,Value)->Bool) {
        super(
            byteCount,
            (val)->{
                var bytes = Bytes.alloc(byteCount);
                if(!writeToBytes(bytes, val))return None;
                return bytes.create();
            },
            (bytes) -> {
                return readFromByte(bytes);
            }
        );
    }
}
class NumberFromByteValueType implements ValueType {
    private var set: (Value)->Option<Bytes>;
    private var get: (Bytes)->Value;
    private var bytes: Int;
    public function new(bytes: Int, set: (Value)->Option<Bytes>, get: (Bytes)->Value){
        this.set = set;
        this.get = get;
        this.bytes = bytes;
    }

    public function read(input:LocalizedInput):ReadValue {
        var readBytes = input.previewNextBytes(bytes);
        try {
            var value : Value = get(readBytes);
            return {
                value: cast value,
                countReadBytes: bytes,
                success: true,
            }
        } catch(_: Exception) {
            return {
                value: NullVal,
                countReadBytes: 0,
                success: false,
            }
        }
    }

    public function write(output: LocalizedOutput, toWrite:Value):Bool {
        var result = false;
        this.set(toWrite).ifPresent((bytes)->{result = output.writeBytes(bytes);});
        return result;
    }

    public function getExpected():Array<String> { return []; }

    public function testWrite(toTest:Value):Bool {
        return this.set(toTest).isPresent();
    }
}

class HexStringToIntValueType extends RegexStringValueType {
    private var strLen: Int;
    public function new(strLen: Int) {
        super(new EReg('[0-9A-Fa-f]{${strLen}}', ""), strLen);
        this.strLen = strLen;
    }

    override function read(input:LocalizedInput):ReadValue {
        var result = super.read(input);
        if(result.success){
            switch(result.value){
                case StrVal(str): return {
                    value: Int64Val(str.hexToInt64()),
                    countReadBytes: result.countReadBytes,
                    success: true,
                };
                default: return {
                    value: result.value,
                    countReadBytes: result.countReadBytes,
                    success: false,
                };
            }
        } else return result;
    }
    override function write(output: LocalizedOutput, toWrite:Value):Bool {
        switch(toWrite){
            case IntVal(val): return super.write(output, StrVal(val.hex(strLen)));
            case Int64Val(val): return super.write(output, StrVal(val.low.hex(strLen)));
            case StrVal(val):
                if(val.length != 1)return false;
                return super.write(output, StrVal(val.charCodeAt(0).hex(strLen)));
            default: return false;
        }
    }
    override function testWrite(toTest:Value):Bool {
        switch(toTest){
            case IntVal(val): return super.testWrite(StrVal(val.hex(strLen)));
            case Int64Val(val): return super.testWrite(StrVal(val.low.hex(strLen)));
            case StrVal(val):
                if(val.length != 1)return false;
                return super.testWrite(StrVal(val.charCodeAt(0).hex(strLen)));
            default: return false;
        }
    }
    override function getExpected():Array<String> {
        return ['$strLen-Hex-Number'];
    }
}
class IntFromTextValueType extends RegexStringValueType {
    public function new() {
        super(~/[+-]?(0[xX][0-9A-Fa-f]+|[0-9]+)/);
    }
    override function read(input:LocalizedInput):ReadValue {
        var result = super.read(input);
        if(result.success){
            switch(result.value){
                case StrVal(str): return {
                    value: IntVal(Std.parseInt(str)),
                    countReadBytes: result.countReadBytes,
                    success: true,
                };
                default: return {
                    value: result.value,
                    countReadBytes: result.countReadBytes,
                    success: false,
                };
            }
        } else return result;
    }
    override function write(output: LocalizedOutput, toWrite:Value):Bool {
        switch(toWrite){
            case IntVal(val): return super.write(output, StrVal('$val'));
            case Int64Val(val): return super.write(output, StrVal('$val'));
            default: return false;
        }
    }
    override function getExpected():Array<String> {
        return ["Int"];
    }

    override function testWrite(toTest:Value):Bool {
        switch(toTest){
            case IntVal(val): return super.testWrite(StrVal('$val'));
            case Int64Val(val): return super.testWrite(StrVal('$val'));
            default: return false;
        }
    }
}
class FloatFromTextValueType extends RegexStringValueType {
    public function new() {
        super(~/[+-]?([0-9]*[.][0-9]+)([eE][+-]?[0-9]+)?/);
    }
    override function read(input:LocalizedInput):ReadValue {
        var result = super.read(input);
        if(result.success){
            switch(result.value){
                case StrVal(str): return {
                    value: FloatVal(Std.parseFloat(str)),
                    countReadBytes: result.countReadBytes,
                    success: true,
                };
                default: return {
                    value: result.value,
                    countReadBytes: result.countReadBytes,
                    success: false,
                };
            }
        } else return result;
    }

    private function floatToString(f:Float) : String {
        if(~/\./.match('$f')) return '$f';
        return '$f.0';
    }
    override function write(output: LocalizedOutput, toWrite:Value):Bool {
        switch(toWrite){
            case FloatVal(val): return super.write(output, StrVal(floatToString(val)));
            default: return false;
        }
    }
    override function testWrite(toTest:Value):Bool {
        switch(toTest){
            case FloatVal(val): return super.testWrite(StrVal(floatToString(val)));
            default: return false;
        }
    }

    override function getExpected():Array<String> {
        return ["Float"];
    }
}

class RegexStringValueType implements ValueType {
    private var regex: EReg;
    private var chunkSize : Int;
    public function new(regex: EReg, chunkSize : Int = 5) {
        this.regex = regex;
        this.chunkSize = chunkSize;
        if(this.chunkSize < 1)this.chunkSize = 1;
    }

    public function read(input:LocalizedInput):ReadValue {
        var currentSize = 0;
        var lastLength = 0;
        do{
            currentSize += this.chunkSize;
            var read = input.previewNextChars(currentSize);
            if(!regex.match(read))break;
            var matched = regex.matchedPos();
            if(matched.pos != 0)break;
            if(matched.len == lastLength){
                return {
                    value: StrVal(read.substring(0, matched.len)),
                    countReadBytes: matched.len,
                    success: true,
                }
            }
            lastLength = matched.len;
        }while(!input.isEof());
        return {
            value: StrVal(""),
            countReadBytes: 0,
            success: false,
        }
    }

    public function write(output: LocalizedOutput, toWrite:Value):Bool {
        switch(toWrite){
            case StrVal(val):
                if(!regex.match(val))return false;
                var matched = regex.matchedPos();
                if(matched.pos != 0 || matched.len != val.length)return false;
                return output.writeString(val);
            default: return false;
        }
    }

    public function getExpected():Array<String> { return ['RegexVal($regex)']; }

    public function testWrite(toTest:Value):Bool {
        switch(toTest){
            case StrVal(val):
                if(!regex.match(val))return false;
                var matched = regex.matchedPos();
                if(matched.pos != 0 || matched.len != val.length)return false;
                return true;
            default: return false;
        }
    }
}

class AbstractValueType implements ValueType {
    private var readFun:(LocalizedInput)->ReadValue;
    private var writeFun:(LocalizedOutput,Value)->Bool;
    private var testFun:(Value)->Bool;
    private var expected:Array<String>;
    public function new(
        readFun:(LocalizedInput)->ReadValue,
        writeFun:(LocalizedOutput,Value)->Bool,
        testFun:(Value)->Bool,
        expected:Array<String>
    ) {
        this.expected = expected;
        this.readFun = readFun;
        this.writeFun = writeFun;
        this.testFun = testFun;
    }

    public function read(input:LocalizedInput):ReadValue {return readFun(input);}
    public function write(output:LocalizedOutput, toWrite:Value):Bool {return writeFun(output,toWrite);}
    public function testWrite(toTest: Value) : Bool {return testFun(toTest);}
    public function getExpected():Array<String> {return expected;}
}

interface ValueType {
    function read(input: LocalizedInput) : ReadValue;
    function write(output: LocalizedOutput, toWrite: Value) : Bool;
    function testWrite(toTest: Value) : Bool;
    function getExpected() : Array<String>;
}
