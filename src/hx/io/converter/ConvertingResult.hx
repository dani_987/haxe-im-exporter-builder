package hx.io.converter;

import hx.streams.ErrorDetail;

using extension.tools.OptionTools;

enum FileAccessorResult<T> {
    Success(val: T);
    WithError(error: ErrorDetail);
}

enum ConvertingResult<T> {
    Success(val: T);
    WithError(error: ErrorDetail);
    WithDataError(error: String);
}
