package hx.io.converter;

import haxe.EnumTools;
import haxe.Constraints.IMap;
import hx.macro.MacroTypeDecorderHelper.DecodedRuntimeType;
import hx.io.converter.FileFormat.Value;

using haxe.Int64;
using haxe.EnumTools.EnumValueTools;

class TypedRuntimeConverter {
    public static function fromValue(toConvert: Value, type: DecodedRuntimeType) : ConvertingResult<Dynamic> {
        if(type.nullable){
            switch (toConvert){
                case NullVal: return Success(null);
                case _:
            }
        }
        switch(type.decoded){
            case TypeInt:
                switch (toConvert){
                    case IntVal(val): return Success(val);
                    case Int64Val(val): return Success(val.low);
                    case _:
                }
            case TypeFloat:
                switch (toConvert){
                    case IntVal(val): return Success(cast(val, Float));
                    case Int64Val(val): return Success(Std.parseFloat(haxe.Int64.toStr(val)));
                    case FloatVal(val): return Success(val);
                    case _:
                }
            case TypeString:
                switch (toConvert){
                    case StrVal(val): return Success(val);
                    case _:
                }
            case TypeBool:
                switch (toConvert){
                    case BoolVal(val): return Success(val);
                    case _:
                }
            case TypeArray(of): return convertIterateableList(toConvert, of, ()->[], (arr,val)->{arr.push(val);arr;});
            case TypeList(of): return convertIterateableList(toConvert, of, ()->new List(), (arr,val)->{arr.add(val);arr;});
            case TypeMap(ofKey, ofValue):
                switch(toConvert){
                    case MapVal(data):
                        var result : Map<Any,Dynamic> = [];
                        for(el in data){
                            switch(fromValue(el.key, ofKey)){
                                case Success(k):
                                    switch(fromValue(el.value, ofValue)){
                                        case Success(v): result[k] = v;
                                        case _ => res: return res;
                                    }
                                case _ => res: return res;
                            }
                        }
                        return Success(result);
                    case _:
                }
            case TypeStruct(fields):
                switch(toConvert){
                    case MapVal(data):
                        var result = {};
                        for(field in fields){
                            var val = data.filter(el -> switch(el){
                                case {key: StrVal(name)}: name == field.name;
                                case _: false;
                            }).map(el -> el.value)[0];
                            if(val == null)return WithDataError('Missing field ${field.name} in $data');
                            switch(fromValue(val, field)){
                                case Success(v): Reflect.setField(result, field.name, v);
                                case _ => res: return res;
                            }
                        }
                        return Success(result);
                    case _:
                }
            case TypeEnum(posibilities):
                switch(toConvert){
                    case EnumObject(null,_,_,_): return WithDataError('Missing Enumname of $toConvert');
                    case EnumObject(name, val, strVal, params):
                        var enumType = Type.resolveEnum(name);
                        if(enumType == null)return WithDataError('Cannot find Enum $name');
                        var convertedParams : Array<Dynamic> = null;
                        var enumValName = strVal;
                        if(strVal == null){
                            if(val == null)return WithDataError('EnumValue is missing the Id or Name in $toConvert');
                            if(val < 0)return WithDataError('EnumValue needs to have an positive Id for $toConvert');
                            var constr = EnumTools.getConstructors(enumType);
                            if(val >= constr.length)return WithDataError('Enum $name has only ${constr.length} Constructors, but Id of $toConvert is $val');
                            enumValName = constr[val];
                        }
                        var paramTypes = posibilities[enumValName];
                        if(paramTypes == null)return WithDataError('Enum $name has not an Constructor named $enumValName, but is set in $toConvert');
                        if(params.length > 0){
                            convertedParams = [];
                            if(paramTypes.length != params.length)return WithDataError('Enum $name Value $enumValName requires ${paramTypes.length} params, but got ${params.length} in $toConvert');
                            for(i=>param in params){
                                switch(fromValue(param,paramTypes[i])){
                                    case Success(p): convertedParams.push(p);
                                    case _ => res: return res;
                                }
                            }
                        }
                        return Success(Type.createEnum(enumType, strVal, convertedParams));
                    case _:
                }

        }
        return WithDataError('Cannot convert $toConvert to ${type.decoded.getName()} (${type.typeName})');
    }

    private static function convertIterateableList<T>(
        toConvert: Value,
        type: DecodedRuntimeType,
        init: ()->T,
        append: (T,Dynamic)->T
    ) : ConvertingResult<Dynamic> {
        switch(toConvert){
            case ArrVal(val):
                var result = init();
                for(el in val){
                    switch(fromValue(el, type)){
                        case Success(v): result = append(result,v);
                        case _ => res: return res;
                    }
                }
                return Success(result);
            case _:
                return WithDataError('Cannot convert $toConvert to ${type.typeName}');
        }
    }

    public static function toValue(from: Dynamic, type: DecodedRuntimeType) : ConvertingResult<Value> {
        if(from == null){
            if(type.nullable)return Success(NullVal);
            return WithDataError('Value is not Nullable, but is null');
        }
        switch(type.decoded){
            case TypeInt:
                if(from is Int)return Success(IntVal(from));
                else if(from.isInt64())return Success(Int64Val(from));
            case TypeFloat:
                if(from is Float)return Success(FloatVal(from));
            case TypeString:
                if(from is String)return Success(StrVal(from));
            case TypeBool:
                if(from is Bool)return Success(BoolVal(from));
            case TypeArray(of) | TypeList(of): return convertList(from, of);
            case TypeMap(ofKey, ofValue):
                if(from is IMap){
                    var resMap : Array<{key:Value, value:Value}> = [];
                    var mapData = cast(from, IMap<Dynamic,Dynamic>);
                    for(k in mapData.keys()){
                        switch(toValue(k, ofKey)){
                            case Success(convK):
                                switch(toValue(mapData.get(k), ofValue)){
                                    case Success(convV): resMap.push({key:convK, value:convV});
                                    case _ => err: return err;
                                }
                            case _ => err: return err;
                        }
                    }
                    return Success(MapVal(resMap));
                }
            case TypeStruct(fields):
                if(Reflect.isObject(from)){
                    var resMap : Array<{key:Value, value:Value}> = [];
                    for(field in fields){
                        switch(toValue(Reflect.field(from, field.name), field)){
                            case Success(conv): resMap.push({key:StrVal(field.name), value:conv});
                            case _ => res: return res;
                        }
                    }
                    return Success(MapVal(resMap));
                }
            case TypeEnum(posibilities):
                if(Reflect.isEnumValue(from)){
                    var name = Type.getEnum(from).getName();
                    if(name == null)return WithDataError('Cannot get Enum-Name of $from');
                    var params : Array<Value> = [];
                    var enumValName = EnumValueTools.getName(from);
                    var paramTypes = posibilities[enumValName];
                    if(paramTypes == null)return WithDataError('Enum $name has not an Constructor named $enumValName, but is set in $from');
                    for(i=>param in EnumValueTools.getParameters(from)){
                        switch(toValue(param, paramTypes[i])){
                            case Success(conv): params.push(conv);
                            case _ => res: return res;
                        }
                    }
                    return Success(EnumObject(
                        name,
                        EnumValueTools.getIndex(from),
                        enumValName,
                        params
                    ));
                }
        }
        return WithDataError('Cannot convert $from to ${type.decoded.getName()} (${type.typeName})');
    }

    private static function convertList(from: Dynamic, type: DecodedRuntimeType) : ConvertingResult<Value> {
        if(from is Array) {
            var resArr = [];
            for(el in cast(from,Array<Dynamic>)){
                switch(toValue(el, type)){
                    case Success(conv): resArr.push(conv);
                    case _ => err: return err;
                }
            }
            return Success(ArrVal(resArr));
        } else if(from is List){
            var resArr = [];
            for(el in cast(from,List<Dynamic>)){
                switch(toValue(el, type)){
                    case Success(conv): resArr.push(conv);
                    case _ => err: return err;
                }
            }
            return Success(ArrVal(resArr));
        } else return WithDataError('$from should be a List or an Array!');
    }
}
