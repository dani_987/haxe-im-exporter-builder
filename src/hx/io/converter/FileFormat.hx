package hx.io.converter;

import haxe.io.Bytes;
import haxe.ds.Option;
import haxe.Int64;
import hx.string.StringBuilder;

using extension.tools.DefaultTools;
using extension.tools.OptionTools;
using hx.io.converter.FileFormat.ValueTools;

enum OneOf<A,B>{
    First(val:A);
    Second(val:B);
}

enum WriterEnumType {
    Both;
    IntOnly;
    StringOnly;
}

enum FileFormat {
    //Const Value
    ConstVal(type: ValueType, value: Value);
    ConstValConverter(type: ValueType, value: Value, result: Value);
    //Getting/Writing Data
    ConvertValue(type: ValueType);
    //Merging Data
    OneOf(of: Array<FileFormat>);
    AllOrdered(ordered: Array<FileFormat>);
    Optional(optional: FileFormat);
    RepeatCount(toRepeat: FileFormat, count: Int);
    RepeatMax(toRepeat: FileFormat);
    Merge(toMerge: FileFormat, merger: DataOperationFunction);
    //Dynamic
    DynamicFormat(basedOn:FileFormat, generate: (Option<Value>)->OneOf<FileFormat,Array<String>>);

    //Ignoring some signs for Parsing Human readable Files
    ReadIgnoringBetweenTokens(applyOn: FileFormat, toIgnore: ValueType, ?defaultValue:Value);
    ReadWithoutIgnoring(applyOn: FileFormat, toIgnore: ValueType);
    IgnoreOnce(toIgnore: ValueType, ?defaultValue:Value);

    //Add a Constant only during writing
    WriteOnlyConstVal(type: ValueType, value: Value);
}

typedef DataOperationFunction = {
    onParse: Value->Value,
    onCreate: Value->Option<Value>,
}

class FileFormatHelperTools {
    public static function sorted(of: Array<{key:Value, value:Value}>) : Array<{key:Value, value:Value}> {
        var copy = of.copy();
        copy.sort((a,b)->a.key.compare(b.key));
        return copy;
    }
}

class FileFormatTools {
    public static function getMapItemWriteOrderer(requiredOrder: Array<Value>) : DataOperationFunction{
        return {
            onParse: v->v,
            onCreate: (v)->{
                switch(v){
                    case MapVal(arr):
                        var newArr = [];
                        for(key in requiredOrder){
                            var item = arr.filter(el->el.key.equals(key)).shift();
                            if(item == null)return None;
                            newArr.push(item);
                        }
                        return Some(MapVal(newArr));
                    default: return None;
                }
            }
        }
    }
    public static function getSingleItemListToItemMapper() : DataOperationFunction{
        return {
            onParse: (from)->{
                switch(from){
                    case ArrVal([it]): return it;
                    default: return from;
                }
            },
            onCreate: (item)->Some(ArrVal([item]))
        }
    }
    public static function getListToMapMerger(defaults: Array<{key:Value, value:Value}> = null) : DataOperationFunction{
        return {
            onParse: (from)->{
                switch(from){
                    case ArrVal(arr):
                        var result = [];
                        for(el in arr){
                            switch (el){
                                case ArrVal(a):
                                    if(a.length>=2){
                                        result.push({key:a[0],value:a[1]});
                                    }
                                default:
                            }
                        }
                        if(defaults != null)for(el in defaults){
                            if(result.filter(obj->'${obj.key}'=='${el.key}').length == 0){
                                result.push(el);
                            }
                        }
                        return MapVal(result);
                    default: return from;
                }
            },
            onCreate: (from)->{
                switch(from){
                    case MapVal(data):
                        return Some(ArrVal([for(v in data)ArrVal([v.key,v.value])]));
                    default: return None;
                }
            }
        }
    }
    public static function getListToEnumMerger(writeType: WriterEnumType = StringOnly) : DataOperationFunction{
        return {
            onParse: (from)->{
                switch(from){
                    case ArrVal([StrVal(enumName),StrVal(val)]): return EnumObject(enumName,null,val,[]);
                    case ArrVal([StrVal(enumName),IntVal(val)]): return EnumObject(enumName,val,null,[]);
                    case ArrVal([StrVal(enumName),Int64Val(val)]): return EnumObject(enumName,val.low,null,[]);
                    case ArrVal([StrVal(enumName),StrVal(val),ArrVal(params)]): return EnumObject(enumName,null,val,params);
                    case ArrVal([StrVal(enumName),IntVal(val),ArrVal(params)]): return EnumObject(enumName,val,null,params);
                    case ArrVal([StrVal(enumName),Int64Val(val),ArrVal(params)]): return EnumObject(enumName,val.low,null,params);
                    default: return from;
                }
            },
            onCreate: (from)->{
                switch(from){
                    case EnumObject(name,iVal,sVal,params):
                        if(sVal == null) {
                            switch(writeType){
                                case Both: return None;
                                case IntOnly: return Some(ArrVal([StrVal(name),IntVal(iVal),ArrVal(params)]));
                                case StringOnly: return None;
                            }
                        } else {
                            switch(writeType){
                                case Both:
                                    if(iVal == null) return None;
                                    else return Some(ArrVal([StrVal(name),IntVal(iVal),StrVal(sVal),ArrVal(params)]));
                                case StringOnly: return Some(ArrVal([StrVal(name),StrVal(sVal),ArrVal(params)]));
                                case IntOnly:
                                    if(iVal == null) return None;
                                    else return Some(ArrVal([StrVal(name),IntVal(iVal),ArrVal(params)]));
                            }
                        }
                    default: return None;
                }
            }
        }
    }

    public static function getListToTypedEnumMerger(enumName: String, writeType: WriterEnumType = StringOnly) : DataOperationFunction{
        return {
            onParse: (from)->{
                switch(from){
                    case ArrVal([StrVal(val)]): return EnumObject(enumName,null,val,[]);
                    case ArrVal([IntVal(val)]): return EnumObject(enumName,val,null,[]);
                    case ArrVal([Int64Val(val)]): return EnumObject(enumName,val.low,null,[]);
                    case ArrVal([StrVal(val),ArrVal(params)]): return EnumObject(enumName,null,val,params);
                    case ArrVal([IntVal(val),ArrVal(params)]): return EnumObject(enumName,val,null,params);
                    case ArrVal([Int64Val(val),ArrVal(params)]): return EnumObject(enumName,val.low,null,params);
                    default: return from;
                }
            },
            onCreate: (from)->{
                switch(from){
                    case EnumObject(name,iVal,sVal,params):
                        if(name != enumName)return None;
                        if(sVal == null) {
                            switch(writeType){
                                case Both: return None;
                                case IntOnly: return Some(ArrVal([IntVal(iVal),ArrVal(params)]));
                                case StringOnly: return None;
                            }
                        } else {
                            switch(writeType){
                                case Both:
                                    if(iVal == null) return None;
                                    else return Some(ArrVal([IntVal(iVal),StrVal(sVal),ArrVal(params)]));
                                case StringOnly: return Some(ArrVal([StrVal(sVal),ArrVal(params)]));
                                case IntOnly:
                                    if(iVal == null) return None;
                                    else return Some(ArrVal([IntVal(iVal),ArrVal(params)]));
                            }
                        }
                    default: return None;
                }

            }
        }
    }

    public static function getEnumPacker(enumValue: String, enumName: String = null) : DataOperationFunction{
        return {
            onParse: (from)->{
                return EnumObject(enumName,null,enumValue,[from]);
            },
            onCreate: (from)->{
                switch(from){
                    case EnumObject(en,_,ev,[val]):
                        if(enumName != null && en != null && en != enumName)return None;
                        if(ev != enumValue)return None;
                        return Some(val);
                    default: return None;
                }
            }
        }
    }

    public static function getOptPlusListToListMerger(valBeforeArr: Bool = true) : DataOperationFunction{
        var merge = (v:Value, arr:Array<Value>) -> {
            var newArray = [];
            if(valBeforeArr)newArray.push(v);
            for(item in arr){
                newArray.push(item);
            }
            if(!valBeforeArr)newArray.push(v);
            return ArrVal(newArray);
        };
        return {
            onParse: (from)->{
                if(valBeforeArr){
                    switch(from){
                        case ArrVal([v,ArrVal(arr)]):
                            return merge(v, arr);
                        default: return from;
                    }
                } else {
                    switch(from){
                        case ArrVal([ArrVal(arr), v]):
                            return merge(v, arr);
                        case ArrVal([ArrVal(arr)]):
                            return ArrVal(arr);
                        default: return from;
                    }

                }
            },
            onCreate: (from)->{
                switch(from){
                    case ArrVal([]): return Some(ArrVal([]));
                    case ArrVal(arr):
                        var newArr = [];
                        var item = if(valBeforeArr) 0 else arr.length - 1;
                        for(index in 0...arr.length){
                            if(item != index) newArr.push(arr[index]);
                        }
                        if(valBeforeArr) return Some(ArrVal([arr[item], ArrVal(newArr)]));
                        else if(arr.length > 1) return Some(ArrVal([ArrVal(newArr), arr[item]]));
                        else return Some(ArrVal([ArrVal(arr)]));
                    default: return None;
                }
            }
        }
    }

    public static function getStringMerger(splittTester: Null<(String, String)->Bool> = null) : DataOperationFunction {
        return {
            onParse: (val)->{
                switch(val){
                    case ArrVal(arr):
                        var result = new StringBuilder();
                        for(item in arr){
                            switch(item){
                                case StrVal(s):result.add(s);
                                case IntVal(charCode):result.add(String.fromCharCode(charCode));
                                case Int64Val(charCode):result.add(String.fromCharCode(charCode.low));
                                default:
                            }
                        }
                        return StrVal(result.toString());
                    default: return val;
                }
            },
            onCreate: (val)->{
                switch(val){
                    case StrVal(s):
                        var actualString = "";
                        var arr = new List<String>();
                        for(char in s.split("")){
                            if(splittTester.orDefault((_,_)->{return true;})(actualString, char)){
                                arr.add('$actualString$char');
                                actualString = "";
                            }
                            else actualString += char;
                        }
                        return Some(ArrVal([for(item in arr)StrVal(item)]));
                    default: return None;
                }

            },
        }
    }

    public static function getDynamicLenOperator() : DataOperationFunction {
        return {
            onParse: (v)->{
                switch(v){
                    case ArrVal([_,ret]): return ret;
                    default: return v;
                }
            },
            onCreate: (v)->{
                switch(v){
                    case ArrVal(arr): return Some(ArrVal([IntVal(arr.length),ArrVal(arr)]));
                    case MapVal(arr): return Some(ArrVal([IntVal(arr.length),MapVal(arr)]));
                    case StrVal(str): return Some(ArrVal([IntVal(Bytes.ofString(str).length),StrVal(str)]));
                    default: return None;
                }
            },
        }
    }

    public static function tryOperation(toTry:Array<DataOperationFunction>) : DataOperationFunction {
        return {
            onParse: (v) -> {
                for(fun in toTry){
                    try {
                        var res = fun.onParse(v);
                        if(! v.equals(res))return res;
                    } catch(_){}
                }
                return v;
            },
            onCreate: (v) -> {
                for(fun in toTry){
                    try {
                        var res = fun.onCreate(v);
                        if(res.isPresent()){
                            return res;
                        }
                    } catch(_){}
                }
                trace(v);
                return v.create();
            },
        }
    }
    public static function multipleOrderedOperations(ops:Array<DataOperationFunction>) : DataOperationFunction {
        var opsRev = ops.copy();
        opsRev.reverse();
        return {
            onParse: (v) -> {
                var res = v;
                for(fun in ops){
                    res = fun.onParse(res);
                }
                return res;
            },
            onCreate: (v) -> {
                var res = v.create();
                for(i=>fun in opsRev){
                    var resBefore = res.getOrThrow();
                    res = fun.onCreate(resBefore);
                    if(!res.isPresent()){
                        return None;
                    }
                }
                return res;
            },
        }
    }

    public static function getListOrderChanger(len: Int, newOrder: Array<Int>) : DataOperationFunction {
        if(newOrder.filter(pos->(pos>=len || pos < 0)).length > 0)throw 'newOrder-Array $newOrder has illegal arguments';
        return {
            onParse: (v) -> {
                switch(v){
                    case ArrVal(arr):
                        if(arr.length < len)return v;
                        var result = [];
                        for(pos in newOrder){
                            result.push(arr[pos]);
                        }
                        return ArrVal(result);
                    default: return v;
                }
            },
            onCreate: (v) -> {
                switch(v){
                    case ArrVal(arr):
                        if(arr.length < len)return None;
                        var resultMap : Array<{pos:Int,val:Value}> = [];
                        for(oldPos => newPos in newOrder){
                            resultMap.push({pos: newPos, val:arr[oldPos]});
                        }
                        resultMap.sort((a,b)->Reflect.compare(a.pos,b.pos));
                        return Some(ArrVal(resultMap.map(el->el.val)));
                    default: return None;
                }
            },
        }
    }
    public static function getListToSublistedList(startPos: Int, lenOfSubItems: Int) : DataOperationFunction {
        return {
            onParse: (v) -> {
                switch(v){
                    case ArrVal(arr):
                        if(arr.length < startPos + lenOfSubItems)return v;
                        var result = arr.slice(0,startPos);
                        result.push(ArrVal(arr.slice(startPos,startPos+lenOfSubItems)));
                        for(el in arr.slice(startPos+lenOfSubItems)){
                            result.push(el);
                        }
                        return ArrVal(result);
                    default: return v;
                }
            },
            onCreate: (v) -> {
                switch(v){
                    case ArrVal(arr):
                        if(arr.length < startPos + 1)return None;
                        var result = arr.slice(0,startPos);
                        switch(arr[startPos]){
                            case ArrVal(a):
                                if(a.length < lenOfSubItems)return None;
                                for(el in a.slice(0,lenOfSubItems)){
                                    result.push(el);
                                }
                            case _: return None;
                        }
                        for(el in arr.slice(startPos+1)){
                            result.push(el);
                        }
                        return Some(ArrVal(result));
                    default: return None;
                }
            },
        }
    }

    public static function getSublistedListToList(startPos: Int, lenOfSubItems: Int) : DataOperationFunction {
        var listDivider = getListToSublistedList(startPos, lenOfSubItems);
        return {
            onParse: (v) -> {
                switch(listDivider.onCreate(v)){
                    case Some(res): return res;
                    default: return v;
                }
            },
            onCreate: (v) -> {
                var res = listDivider.onParse(v);
                if(res.equals(v)) return None;
                return Some(res);
            },
        }
    }

}

enum Value {
    StrVal(val: String);
    BoolVal(val: Bool);
    IntVal(val: Int);
    Int64Val(val: Int64);
    FloatVal(val: Float);
    ArrVal(val: Array<Value>);
    MapVal(val: Array<{key:Value, value:Value}>);
    EnumObject(name: Null<String>, val: Null<Int>, strVal: Null<String>, params: Array<Value>);
    NullVal;
}

class ValueTools {
    public static function equals(v1: Value, v2: Value) : Bool {
        switch([v1,v2]){
            case [StrVal(s1),StrVal(s2)]: return s1==s2;
            case [BoolVal(b1),BoolVal(b2)]: return b1==b2;
            case [IntVal(i1),IntVal(i2)]: return i1==i2;
            case [Int64Val(i1),Int64Val(i2)]: return Int64.compare(i1,i2) == 0;
            case [Int64Val(i1),IntVal(i2)]: return Int64.compare(i1,Int64.ofInt(i2)) == 0;
            case [IntVal(i1),Int64Val(i2)]: return Int64.compare(Int64.ofInt(i1),i2) == 0;
            case [FloatVal(f1),FloatVal(f2)]: return Math.abs(f1-f2) < 1e-7;
            case [ArrVal(a1),ArrVal(a2)]:
                if(a1.length != a2.length)return false;
                for(i=>_ in a1){
                    if(!equals(a1[i],a2[i]))return false;
                }
                return true;
            case [MapVal(m1),MapVal(m2)]:
                if(m1.length != m2.length)return false;
                for(i=>_ in m1){
                    if(!equals(m1[i].key,m1[i].key))return false;
                    if(!equals(m1[i].value,m2[i].value))return false;
                }
                return true;
            case [EnumObject(n1, v1, s1, p1),EnumObject(n2, v2, s2, p2)]:
                if(n1 != null && n2 != null && n1!=n2)return false;
                if(s1 != null && s2 != null && s1!=s2)return false;
                if(v1 != null && v2 != null && v1!=v2)return false;
                if(p1.length != p2.length)return false;
                for(i=>_ in p1){
                    if(!equals(p1[i],p2[i]))return false;
                }
                return true;
            case [NullVal,NullVal]: return true;
            default: return false;
        }
    }

    public static function compare(v1: Value, v2: Value) : Int {
        switch([v1,v2]){
            case [StrVal(s1),StrVal(s2)]: return Reflect.compare(s1,s2);
            case [BoolVal(b1),BoolVal(b2)]: return Reflect.compare(b1,b2);
            case [IntVal(i1),IntVal(i2)]: return Reflect.compare(i1,i2);
            case [Int64Val(i1),Int64Val(i2)]: return Int64.compare(i1,i2);
            case [Int64Val(i1),IntVal(i2)]: return Int64.compare(i1,Int64.ofInt(i2));
            case [IntVal(i1),Int64Val(i2)]: return Int64.compare(Int64.ofInt(i1),i2);
            case [FloatVal(f1),FloatVal(f2)]: return if(Math.abs(f1-f2) < 1e-7) 0 else return Reflect.compare(f1,f2);
            case [ArrVal(a1),ArrVal(a2)]:
                if(a1.length != a2.length)return Reflect.compare(a1.length, a2.length);
                for(i=>_ in a1){
                    if(!equals(a1[i],a2[i]))return compare(a1[i],a2[i]);
                }
                return 0;
            case [MapVal(m1),MapVal(m2)]:
                if(m1.length != m2.length)return Reflect.compare(m1.length, m2.length);
                for(i=>_ in m1){
                    if(!equals(m1[i].key,m1[i].key))compare(m1[i].key,m1[i].key);
                    if(!equals(m1[i].value,m2[i].value))compare(m1[i].value,m2[i].value);
                }
                return 0;
            case [EnumObject(n1, v1, s1, p1),EnumObject(n2, v2, s2, p2)]:
                if(n1 != null && n2 != null && n1!=n2)return Reflect.compare(n1, n2);
                if(s1 != null && s2 != null && s1!=s2)return Reflect.compare(s1, s2);
                if(v1 != null && v2 != null && v1!=v2)return Reflect.compare(v1, v2);
                if(p1.length != p2.length)return Reflect.compare(p1.length, p2.length);
                for(i=>_ in p1){
                    if(!equals(p1[i],p2[i]))return compare(p1[i],p2[i]);
                }
                return 0;
            case [NullVal,NullVal]: return 0;
            default: return Reflect.compare(v1.getIndex(), v2.getIndex());
        }
    }
}
