package hx.io.converter;

#if macro
import haxe.macro.ExprTools;
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
import hx.macro.MacroTypeHelper;
import hx.io.converter.FileFormat;

using hx.macro.MacroTypeDecorderHelper;
using hx.macro.MacroDefineHelper;
using haxe.macro.Tools;

typedef SwitchCases = Array<{name:String, params:Array<Expr>, expr:Expr}>;

class StructureConverterMacro {
    static function build() : ComplexType {
        switch (Context.getLocalType()) {
            case TInst(_.get() => {name: name, pack: pack}, params):
                return buildFunctions(pack, name, params);
            default:
                throw 'Illegal Type ${Context.getLocalType()}!';
        }
    }

    private static function buildFunctions(
        pack: Array<String>, name: String, params:Array<Type>
    ) : ComplexType{
        var numParams = params.length;
        var newName = '${name}__${params.map((param) -> {param.toUniqueName();}).join('_I_')}';

        if (!MacroTypeDecorderHelper.isDefinedAsTypeInPath(newName, pack)) {
			Context.defineType({
				pack: pack,
				name: newName,
				pos: MacroDefineHelper.getPosition(),
				params: [for (i in 0...numParams){name: 'T$i'}],
				kind: TDClass(),
				fields: getFields(params, [for (t in params) TPType(t.toComplexType())]),
			});
		}
        return TPath({pack: pack, name: newName, params: [for (t in params) TPType(t.toComplexType())]});
    }

    private static function getFields(paramTypes:Array<Type>, params:Array<TypeParam>) : Array<Field> {
        if(params.length == 0)throw "Missing Parameter";
        if(params.length > 1)throw "Only 1 Parameter is allowed";
        var path = ["hx","io","gen","converter"];
        var funName = "convert";
        var decoded = paramTypes[0].decode();
        var typeDef = decoded.asTypedExpr(macro : hx.macro.MacroTypeDecorderHelper.DecodedRuntimeType);
        var fields : Array<Field> = [];
        fields.push("reader".toVariable(macro: FileAccessor));
        fields.push("typeDefinition".toVariable(macro: hx.macro.MacroTypeDecorderHelper.DecodedRuntimeType));

		fields.push("new".toPublicFunction([
            {name: 'fileFormat', type: macro: hx.io.converter.FileFormat}
        ], macro {
            this.reader = new FileAccessor(fileFormat);
            this.typeDefinition = $typeDef;
        }, macro : Void));

        fields.push("parse".toPublicFunction([
			{name: 'input', type: macro : hx.streams.LocalizedInput}
		], macro {
			switch(this.reader.parseFromInput(input)){
                case Success(data): return cast TypedRuntimeConverter.fromValue(data, typeDefinition);
                case WithError(error): return WithError(error);
            };
		}, TPath({
			pack: ["hx", "io", "converter"],
			name: "ConvertingResult",
			params: params,
		})));

        fields.push("write".toPublicFunction([
            {name: 'output', type: macro: hx.streams.LocalizedOutput},
            {name: 'data', type: paramTypes[0].toComplexType()},
        ], macro {
            switch(TypedRuntimeConverter.toValue(data, typeDefinition)){
                case Success(v):
                    switch(this.reader.writeToOutput(output, v)){
                        case Some(err): trace(err); return Some(err);
                        case None: return None;
                    }
                case WithDataError(error): return Some(error);
                case WithError(error): return Some(error.message);
            }
        }, macro : haxe.ds.Option<String>));

		return fields;
    }
}
#end
