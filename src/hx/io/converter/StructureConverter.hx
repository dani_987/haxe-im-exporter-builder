package hx.io.converter;

import haxe.ds.Option;
import hx.streams.LocalizedInput;
import hx.streams.LocalizedOutput;
import hx.io.converter.FileFormat;
import hx.io.converter.ConvertingResult;
import hx.macro.MacroTypeDecorderHelper.DecodedRuntimeType;

class StructureConverter {
    private var reader: FileAccessor;
    private var typeDefinition : DecodedRuntimeType;

    public function new(typeDefinition : DecodedRuntimeType, fileFormat: FileFormat){
        this.reader = new FileAccessor(fileFormat);
        this.typeDefinition = typeDefinition;
    }

    public function parse(input: LocalizedInput) : ConvertingResult<Dynamic> {
        switch(this.reader.parseFromInput(input)){
            case Success(data): return TypedRuntimeConverter.fromValue(data, typeDefinition);
            case WithError(error): return WithError(error);
        };
    }
    public function write(output: LocalizedOutput, data: Dynamic) : Option<String> {
        switch(TypedRuntimeConverter.toValue(data, typeDefinition)){
            case Success(v):
                switch(this.reader.writeToOutput(output, v)){
                    case Some(err): return Some(err);
                    case None: return None;
                }
            case WithDataError(error): return Some(error);
            case WithError(error): return Some(error.message);
        }
    }
}
