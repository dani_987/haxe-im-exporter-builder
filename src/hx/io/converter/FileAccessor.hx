package hx.io.converter;

import haxe.io.Output;
import hx.io.converter.ValueType;
import haxe.PosInfos;
import hx.streams.ErrorDetail.ErrorDetailFollower;
import haxe.ds.Option;
import hx.io.converter.FileFormat.Value;
import hx.io.converter.ConvertingResult.FileAccessorResult;
import hx.streams.LocalizedInput;
import hx.streams.LocalizedOutput;
import hx.streams.PositionReader;

using extension.tools.OptionTools;
using extension.tools.DefaultTools;
using hx.io.converter.FileFormat.ValueTools;

enum Result {
    Success(position: Int, data: Option<Value>, requiredToContinue: ErrorData);
    Error(data: ErrorData);
}

enum WriteResult {
    WrittenData(funList: Array<(LocalizedOutput)->Option<String>>);
    DataWasntWritten(funList: Array<(LocalizedOutput)->Option<String>>);
    Nok;
}

typedef ErrorData = {
    position: Int, required: Array<String>
}

enum RuleType {
    Read;
    Ignore;
}

typedef IgnorRuling = List<{type: RuleType, reader: ValueType}>;

class FileAccessor{
    private var fileFormat: FileFormat;
    public function new(fileFormat: FileFormat) {
        this.fileFormat = fileFormat;
    }

    private static function makeValueType(fileFormat: FileFormat, expected: Null<Array<String>>, writeAllowed: Bool) : ValueType {
        return new AbstractValueType(
            (input)->{
                switch(parse(fileFormat,new PositionReader(input),new List())){
                    case Success(position, Some(data), _):
                        return {value: data, countReadBytes: position, success: true };
                    default:
                }
                return {value: NullVal, countReadBytes: 0, success: false };
            },
            (out,val)->{
                if(!writeAllowed) return false;
                switch(createWriter(fileFormat, val)){
                    case Nok: return false;
                    case WrittenData(funList):
                        return ! write(out, funList, false).isPresent();
                    case DataWasntWritten(funList):
                        return ! write(out, funList, false).isPresent();
                }
            },
            (toTest)->{
                if(!writeAllowed) return false;
                switch(createWriter(fileFormat, toTest)){
                    case Nok: return false;
                    default: return true;
                }
            },
            expected.orDefault([]),
        );
    }

    public static function asValueType(fileFormat: FileFormat, expected: Array<String> = null) : ValueType {
        return makeValueType(fileFormat, expected, false);
    }

    public static function asReadWriteValueType(fileFormat: FileFormat, expected: Array<String> = null) : ValueType {
        return makeValueType(fileFormat, expected, true);
    }


    private static function mergeErrors(e1: ErrorData, e2: ErrorData, ?pos:PosInfos) : ErrorData {
        if(e1.required.length == 0)return e2;
        if(e2.required.length == 0)return e1;
        switch(e1.position - e2.position){
            case _ > 0 => true: return e1;
            case _ < 0 => true: return e2;
            default:
                for(el in e2.required){
                    if(!e1.required.contains(el)){
                        e1.required.push(el);
                    }
                }
                return e1;
        }
    }

    private static function ignore(rules: IgnorRuling, input: PositionReader) : {pos: Int, err: ErrorData} {
        var actualStatus : RuleType = Read;
        var errorData = {position: input.start, required: []};
        var reader = input.clone();
        for(rule in rules){
            if(rule.type == actualStatus)continue;
            var read = rule.reader.read(reader.clone());
            if(read.success){
                actualStatus = rule.type;
                switch(rule.type){
                    case Read:
                        reader = input.clone();
                    case Ignore:
                        reader.moveStart(read.countReadBytes);
                }
            }
        }
        return {pos: reader.start, err: errorData};
    }

    private static function parse(format: FileFormat, input: PositionReader, ignore: IgnorRuling) : Result {
        var ignoreData = FileAccessor.ignore(ignore, input);
        input.start = ignoreData.pos;
        var reader = input.clone();
        switch(format){
            case ConstVal(tester, value):
                var read = tester.read(reader);
                if(read.success && value.equals(read.value)){
                    input.moveStart(read.countReadBytes);
                    return Success(input.start, None, {position: input.start, required: []});
                }
                return Error(mergeErrors(ignoreData.err, {position: input.start, required: ['$value']}));
            case ConstValConverter(tester, value, result):
                var read = tester.read(reader);
                if(read.success && value.equals(read.value)){
                    input.moveStart(read.countReadBytes);
                    return Success(input.start, result.create(), {position: input.start, required: []});
                }
                return Error(mergeErrors(ignoreData.err, {position: input.start, required: ['$value']}));
            case ConvertValue(tester):
                var read = tester.read(reader);
                if(!read.success)return Error(mergeErrors(ignoreData.err, {position: input.start, required: tester.getExpected()}));
                input.moveStart(read.countReadBytes);
                return Success(input.start, read.value.create(), {position: input.start, required: []});
            case OneOf(formatList):
                var errorData = ignoreData.err;
                for(element in formatList){
                    var reader = input.clone();
                    switch(parse(element, reader, ignore)){
                        case Success(pos, readData, req):
                            return Success(pos, readData, mergeErrors(errorData, req));
                        case Error(err):
                            errorData = mergeErrors(errorData, err);
                    }
                }
                return Error(errorData);
            case AllOrdered(ordered):
                var data = [];
                var errorData = ignoreData.err;
                for(step in ordered){
                    switch(parse(step, reader, ignore)){
                        case Success(pos, readData, err):
                            readData.ifPresent((r)->{data.push(r);});
                            reader.start = pos;
                            errorData = mergeErrors(errorData, err);
                        case Error(err):
                            errorData = mergeErrors(errorData, err);
                            return Error(errorData);
                    }
                }
                return Success(reader.start, ArrVal(data).create(), errorData);
            case Optional(opt):
                switch(parse(opt, reader, ignore)){
                    case Success(pos, readData, err):
                        return Success(pos, readData, mergeErrors(ignoreData.err, err));
                    case Error(err):
                        return Success(input.start, None, mergeErrors(ignoreData.err, err));
                }
            case RepeatCount(toRepeat, count):
                var data = [];
                var errorData = ignoreData.err;
                for(_ in 0...count){
                    switch(parse(toRepeat, reader, ignore)){
                        case Success(pos, readData, err):
                            readData.ifPresent((r)->{data.push(r);});
                            errorData = mergeErrors(errorData, err);
                            if(input.start == pos){//Break infinate loop
                                return Error(errorData);
                            }
                            reader.start = pos;
                            input.start = pos;
                        case Error(err):
                            errorData = mergeErrors(errorData, err);
                            return Error(errorData);
                    }
                }
                return Success(input.start, ArrVal(data).create(), errorData);
            case RepeatMax(toRepeat):
                var data = [];
                var errorData = ignoreData.err;
                while(true){
                    switch(parse(toRepeat, reader, ignore)){
                        case Success(pos, readData, err):
                            readData.ifPresent((r)->{data.push(r);});
                            errorData = mergeErrors(errorData, err);
                            if(input.start == pos){//Break infinate loop
                                return Success(input.start, ArrVal(data).create(), errorData);
                            }
                            reader.start = pos;
                            input.start = pos;
                        case Error(err):
                            errorData = mergeErrors(errorData, err);
                            return Success(input.start, ArrVal(data).create(), errorData);
                    }
                }
            case Merge(toMerge, merger):
                switch(parse(toMerge, reader, ignore)){
                    case Success(pos, Some(readData), err):
                        return Success(pos, merger.onParse(readData).create(), mergeErrors(ignoreData.err, err));
                    case Success(pos, None, err):
                        return Success(pos, None, mergeErrors(ignoreData.err, err));
                    case Error(err):
                        return Error(mergeErrors(ignoreData.err, err));
                }
            case DynamicFormat(basedOn, generate):
                switch(parse(basedOn, reader, ignore)){
                    case Success(position, data, requiredToContinue):
                        switch(generate(data)){
                            case First(genFormat):
                                reader.start = position;
                                switch(parse(genFormat, reader, ignore)){
                                    case Success(position, dataGen, requiredToContinueInner):
                                        return Success(position, ArrVal([
                                            data.getOrDefault(NullVal),dataGen.getOrDefault(NullVal)
                                        ]).create(), mergeErrors(ignoreData.err, mergeErrors(requiredToContinue, requiredToContinueInner)));
                                    case Error(data):return Error(data);
                                }
                            case Second(err):
                                return Error(mergeErrors(ignoreData.err,{position: position, required: err}));
                        }
                    case Error(data): return Error(mergeErrors(ignoreData.err, data));
                }

            case ReadIgnoringBetweenTokens(applyOn, toIgnore, _):
                var newIgnore = new List();
                for (value in ignore) {newIgnore.add(value);}
                newIgnore.add({type: Ignore, reader: toIgnore});
                switch(parse(applyOn, reader, newIgnore)){
                    case Success(position, data, err):
                        reader.start = input.start = position;
                        var read = toIgnore.read(reader);
                        if(read.success){
                            input.moveStart(read.countReadBytes);
                            return Success(input.start, data, mergeErrors(ignoreData.err, err));
                        }
                        return Success(input.start, data, mergeErrors(ignoreData.err, err));
                    case Error(err):return Error(mergeErrors(ignoreData.err, err));
                }

            case ReadWithoutIgnoring(applyOn, toIgnore):
                var newIgnore = new List();
                for (value in ignore) {newIgnore.add(value);}
                newIgnore.add({type: Read, reader: toIgnore});
                return parse(applyOn, reader, newIgnore);
            case IgnoreOnce(toIgnore, _):
                var read = toIgnore.read(reader);
                if(read.success){
                    reader.moveStart(read.countReadBytes);
                }
                return Success(reader.start, None, ignoreData.err);

            case WriteOnlyConstVal(type, value):
                return Success(reader.start, None, ignoreData.err);

        }
        return Error(ignoreData.err);
    }

    public function parseFromInput(input: LocalizedInput) : FileAccessorResult<Value> {
        var reader = new PositionReader(input);
        var result = parse(fileFormat, reader, new List());
        var error;
        switch(result){
            case Success(pos,Some(data),err):
                reader.start = pos;
                if(reader.isEof()){
                    return Success(data);
                }
                error = err;
                error.required.push("EOF / End-Of-File");
            case Success(pos,None,err):
                reader.start = pos;
                error = err;
                trace("No result Provided");
            case Error(err):
                error = err;
        }
        reader.start = error.position + 1;
        var error = reader.getError('Unexpected character. Expected: ${error.required.join(", ")}');
        return WithError(error);
    }

    private static function createWriteFunction(writeFun: (LocalizedOutput,Value)->Bool, val: Value, message: String) : (LocalizedOutput)->Option<String> {
        return (out)->{
            if(writeFun(out, val))return None;
            return Some(message);
        };
    }

    private static function createWriter(format: FileFormat, toConvert: Value, usedValues: Null<Array<{v:Value,f:FileFormat}>> = null, ?pos: PosInfos) : WriteResult {
        var newUsedValues : Array<{v:Value,f:FileFormat}>;
        if(usedValues == null){
            newUsedValues = [{v:toConvert,f:format}];
        }
        else if(usedValues.filter(el->el.v.equals(toConvert) && el.f == format).length > 0){
            //Break recursion, if it does nothing
            return Nok;
        } else {
            newUsedValues = usedValues.copy();
            newUsedValues.push({v:toConvert,f:format});
        }
        var convertOperations : Array<(LocalizedOutput)->Option<String>> = [];
        switch(format){
            case ConstVal(type, value):
                return DataWasntWritten([createWriteFunction(type.write,value,'Cannot write $value using $type')]);
            case ConstValConverter(type, value, result):
                if(!result.equals(toConvert))return Nok;
                return WrittenData([createWriteFunction(type.write,value,'Cannot write $value using $type')]);
            case ConvertValue(type):
                if(!type.testWrite(toConvert))return Nok;
                convertOperations.push(createWriteFunction(type.write,toConvert,'Cannot write $toConvert using $type'));
            case OneOf(of):
                var nonwrittenData : Null<Array<LocalizedOutput -> Option<String>>> = null;
                for(fmt in of){
                    switch(createWriter(fmt, toConvert, newUsedValues)){
                        case WrittenData(arr): return WrittenData(arr);
                        case DataWasntWritten(arr):  if(null == nonwrittenData)nonwrittenData = arr;
                        case Nok:
                    }
                }
                if(nonwrittenData != null)return DataWasntWritten(nonwrittenData);
                return Nok;
            case AllOrdered(ordered):
                var arr : Array<Value>;
                switch(toConvert){
                    case ArrVal(val): arr = val;
                    default: return Nok;
                }
                var indexInArr = 0;
                var written = false;
                for(item in ordered){
                    if(arr.length < indexInArr)return Nok;
                    switch(createWriter(item, if(arr.length == indexInArr)NullVal else arr[indexInArr], newUsedValues)){
                        case Nok: return Nok;
                        case WrittenData(funList):
                            for(fun in funList)convertOperations.push(fun);
                            written = true;
                            indexInArr++;
                        case DataWasntWritten(funList):
                            for(fun in funList)convertOperations.push(fun);
                    }
                }
                if(arr.length != indexInArr)return Nok;
                if(!written){
                    return DataWasntWritten(convertOperations);
                }
            case Optional(option):
                switch(createWriter(option, toConvert, newUsedValues)){
                    case WrittenData(arr): return WrittenData(arr);
                    case DataWasntWritten(arr): return DataWasntWritten(arr);
                    case Nok: return DataWasntWritten([]);
                }
            case RepeatCount(toRepeat, count):
                var arr : Array<Value> = [];
                switch(toConvert){
                    case ArrVal(val): arr = val;
                    case NullVal: arr = [];
                    default: return Nok;
                }
                if(arr.length != count){
                    return Nok;
                }
                for(item in arr){
                    switch(createWriter(toRepeat,item,newUsedValues)){
                        case DataWasntWritten(funList): for(fun in funList)convertOperations.push(fun);
                        case WrittenData(funList): for(fun in funList)convertOperations.push(fun);
                        case Nok: return Nok;
                    }
                }
            case RepeatMax(toRepeat):
                var arr : Array<Value> = [];
                switch(toConvert){
                    case ArrVal(val): arr = val;
                    case NullVal: return WrittenData([]);
                    default: return Nok;
                }
                for(item in arr){
                    switch(createWriter(toRepeat,item,newUsedValues)){
                        case DataWasntWritten(funList) | WrittenData(funList):
                            for(fun in funList)convertOperations.push(fun);
                        case Nok: return Nok;
                    }
                }
            case Merge(toMerge, merger):
                switch(merger.onCreate(toConvert)){
                    case Some(merged):
                        return createWriter(toMerge, merged, newUsedValues);
                    case None: return Nok;
                }
            case DynamicFormat(basedOn, generate):
                switch(toConvert){
                    case ArrVal([baseOnVal,genVal]):
                        switch(createWriter(basedOn, baseOnVal, newUsedValues)){
                            case WrittenData(funList) | DataWasntWritten(funList):
                                switch(generate(Some(baseOnVal))){
                                    case First(genFormat):
                                        switch(createWriter(genFormat, genVal, newUsedValues)){
                                            case WrittenData(funListGen) | DataWasntWritten(funListGen):
                                                for(fun in funList)convertOperations.push(fun);
                                                for(fun in funListGen)convertOperations.push(fun);
                                            default: return Nok;
                                        }
                                    default: return Nok;
                                }
                            default: return Nok;
                        }
                    default: return Nok;
                }
            case ReadIgnoringBetweenTokens(applyOn, _, _):
                return createWriter(applyOn, toConvert, newUsedValues);
            case ReadWithoutIgnoring(applyOn, _):
                return createWriter(applyOn, toConvert, newUsedValues);
            case IgnoreOnce(toIgnore, defaultValue):
                if(toIgnore.testWrite(defaultValue)){
                    convertOperations.push(createWriteFunction(toIgnore.write,defaultValue,'Cannot write $defaultValue using $toIgnore'));
                }
                return DataWasntWritten(convertOperations);


            case WriteOnlyConstVal(type, value):
                convertOperations.push(createWriteFunction(type.write,value,'Cannot write $value using $type'));
                return DataWasntWritten(convertOperations);
        }
        return WrittenData(convertOperations);
    }

    private static function write(output: LocalizedOutput, calls: Array<(LocalizedOutput)->Option<String>>, finish: Bool = true) : Option<String> {
        for(call in calls){
            switch(call(output)){
                case None:
                case Some(error): return Some(error);
            }
        }
        if(finish)output.finish();
        return None;
    }

    public function writeToOutput(output: LocalizedOutput, toConvert: Value) : Option<String> {
        var writeFunctions = createWriter(fileFormat,toConvert);
        switch(writeFunctions){
            case DataWasntWritten(_): return Some("Cannot format Value into given FileFormat - Data cannot be output!");
            case WrittenData(funs): return write(output, funs);
            case Nok: return Some("Cannot format Value into given FileFormat!");
        }
    }
}
